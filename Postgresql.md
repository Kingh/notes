﻿# PostgresSQL

---

**What is a database**

- A database is an organized collection of data, that can be manipulated and retrieved.

**Postgres and Relational Database**

- Postgres is a database engine
- SQL (Structured Query Language) is a programming language for interacting with databases. Manages data held in a relational database.

> How data is stored

- Relational databases store data in tables, formed by column and rows.

> What is PostgresSql

- It's an Object-relational database management system.

**PostgreSQL commands**

- typing `\?` into the _psql_ CLI lists commands
- type `\l` to list available databases
- To connect to a database via the CLI type `psql -h localhost -p 5432 -U username database-name`
- To connect to a database while in the CLI type `\c database_name`
- `\d table_name` lists all the columns belonging to the table.
- `\dt` list tables only.
- `\i` executes commands from a file.

## SQL Commands

- Creating a database `CREATE DATABASE test;`

  > **NOTE** DELETING a database also removes the data records it stores.

- Command for deleting a database `DROP DATABASE database_name;`. This deletes all records.
- Command for creating a table:

```
CREATE TABLE table_name (
    Column name + data type + constraints if any
);
```

- To create a table named person run:

```
CREATE TABLE person (
    id int,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    gender VARCHAR(6),
    date_of_birth TIMESTAMP,
);
```

- The above SQL statement create a table called person with all the columns required by the table.
- To create a table named person with constraints:

```
CREATE TABLE person (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50)NOT NULL,
    gender VARCHAR(6) NOT NULL,
    date_of_birth DATE NOT NULL,
);
```

- `DROP TABLE table_name;` deletes the table and all the data records it holds.
- To insert records into a tables:

```
INSERT INTO table_name (
    column1, column2, column3
) VALUES ( value1, value2, value3);
```

- The `BIGSERIAL` auto increments.
- Use [mockaroo](https://mockaroo.com/) to generate dummy data. Then execute command `\i path-to-file` to run the sql in the file.
- To select all the record in a table: `SELECT * FROM table_name`. The `*` is a wildcard for selecting all records in the table.

> To clear psql console run `\! cls` on windows.

- To select and order records from a table: `SELECT * FROM table_name ORDER BY column_name DESC`. The order can be either _DESC_ or _ASC_, the default is _ASC_.
- To get only distinct rows from records: `SELECT DISTINCT column_name FROM table_name`
- Filtering records where column meets a certain criteria: `SELECT * FROM table_name WHERE column_name = 'condition'`
- Filtering using multiple conditions: `SELECT columns FROM table_name WHERE column_name = 'condition' AND column_name = 'condition'`

## Comparison Operators

- The comparison operators allow for the retrieval of records that meet certain criteria:
  - `=` equals to, `<` less than, `<=` less than or equal to, `>=` greater than or equal to and `<>` not equal to.

## Limit, Offset & Fetch

- To limit the number of record returned: `SELECT * FROM table_name LIMIT 5;`
- Offsetting the limit: `SELECT * FROM person OFFSET 5 LIMIT 5;`
- `LIMIT` is not an official sql key word, the sql equivalent is `FETCH`: `SELECT * FROM person OFFSET 5 FETCH FIRST 5 ROW ONLY;`

## IN

- The `IN` keyword takes an array or values, and returns record that fit that criteria: `SELECT * FROM person WHERE column_name IN ('value1', 'value2');`

## BETWEEN

- The `BETWEEN` keyword is used to select data between a range: `SELECT * FROM table_name WHERE column_name BETWEEN 'value' AND 'value';`

## LIKE and iLIKE

- The like operator is used to match text values using wildcards: `SELECT * FROM table_name WHERE column_name/**email*/ LIKE '%.com'`. The percent sign is a wildcard that specifies any number character.
- To specify a specific number of character use underscores: `SELECT * FROM table_name WHERE column_name/**email*/ LIKE '_____@'`, this specifies six characters followed by the @ sign. To ignore case use the `ILIKE` keyword.

## GROUP BY

- The `GROUP BY` allows the grouping of records by column: `SELECT column_name FROM table_name GROUP BY country_of_birth`.

## GROUP BY HAVING

- Used to perform extra filtering: `SELECT column_name FROM table_name GROUP BY country_of_birth HAVING COUNT(*) < 5`

## Basics of Arithmetic Operators

- 
