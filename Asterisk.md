﻿# <color #708090> Asterisk Tutorial: Installation & configuration
____________________

### Asterisk

> **_Asterisk_** is an open source framework for building communications applications.


- **_Asterisk_** turns an ordinary computer into a  communications server

--------------------------------------------------------------------------------------------------------------
___________________________________________________________________________________

### Session Initiation Protocol (SIP)

> SIP is a signalling protocol used for initiating, maintaining and terminating real-time sessions including voice, video and messaging applications

------------------------------------------------------------------------------------------------------------------------------------------
________________________________________________________________________________________________________

#### Asterisk Installation

- Download **_Asterisk_** from: [**_Asterisk_** download link](http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-16-current.tar.gz) <break>
- run the following command in terminal to install **_Asterisk_** dependencies: 
`sudo apt-get install build-essential wget libssl-dev libncurses5-dev libnewt-dev libxml2-dev linux-headers-$(uname -r) libsqlite3-dev uuid-dev` <break>
- installation commands: <break>
 `sudo tar zxvf asterisk-16-current.tar.gz` <break>
 `cd asterisk-16.6.2/` <break>
 `./configure`  _command to check if all  **Asterisk** dependencies are installed on your system._<break>
 `make menu config` _optional command. Used to open a menu where to select features to install._ <break>
 `make` _compiles **Asterisk**_ <break>
 `make install` _installs **Asterisk** on the linux system_

 --------------------------------------------------------------------------
 ________________________________________________________
 
#### Configuration

- configuration commands: <break>
`cd /etc/asterisk` _configuration files location_ <break>
`cd asterisk-16.6.2/` <break>
`sudo make samples` _creates sample configuration files in <color #00FFFF>'/etc/asterisk'_
<break>`sudo asterisk -cvvv` _start **_Asterisk_**, '-c' get asterisk console and 'vvv' verbose level. <break>
- <color #00FFFF> _**Ctrl + c** stops the **Asterisk** cli_ <break>
- `cd contrib/ini.d/` _folder for **Asterisk** init scripts_
- `sudo cp rc.debian.asterisk /etc/init.d/asterisk` _creates **Asterisk** start script_
- `vim /etc/init.d/asterisk`
- set **DAEMON** in the asterisk file to the location of you **_Asterisk_** binary i.e <color #00FFFF> _'/usr/sbin/asterisk'_
- to find **_Asterisk_** binary location run `type asterisk`
- set **ASTVARRUNDIR** in the asterisk file to <color #00FFFF>_'/var/run/asterisk'_
- and set **ASTETCDIR** in the asterisk file to <color #00FFFF>_'/etc/asterisk'_
- save file and exit
- run `/etc/init.d/asterisk start` to start **_Asterisk_**.  This allows **_Asterisk_** to run as _daemon_ process.
- to log into the running **_Asterisk_**'s _cli_ run `asterisk -r`
- to exit the _cli_ type _exit_ and press _enter_

-------------------------------------------------------------

##### Add **_Asterisk_** User

- run `/etc/init.d/asterisk stop` if **_Asterisk_** is running.
- run `useradd -d /var/lib/asterisk asterisk` to add the asterisk user
- run `chown -R /var/spool/asterisk /var/lib/asterisk /var/run/asterisk` to change file owner from root to asterisk user.
- from the asterisk directory run `cd contrib/init.d`
- run `cp etc_default_asterisk /etc/default/asterisk`, this create a script that wil be called by the init script.
- run `vim /etc/default/asterisk` to open the file for editing
- in the file uncomment the **AST_USER='asterisk'**  and the **AST_GROUP='asterisk'** lines


