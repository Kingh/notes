﻿# Express

- Express is a fast, unopinionated and minimalist web framework for NodeJs
- Express is a **_server-side_** framework. It is not comparable to client-side frameworks like React or Angular. It can be used in combination with those frameworks to build full stack applications.

### Why Express?

- Simplifies building web applications with NodeJs
- Used for both server rendered apps as well as API/Microservices
- Extremely light and fast
- Full control of requests and responses
- Great to use with client side frameworks as it's all JavaScript

### Basic Server Syntax (Express)

```
const express = require('express');

// Init express
const app = express();

// Create endpoints/route handlers
app.get('/', (req, res) => {
    res.send(`Hello World!`);
});

// Assign port
app.listen(5000);
```

### Basic Route Handling

- Handling requests/routes is simple
- app.get(), app.post, app.put(), app.delete(), etc
- Access to params, query string, url parts, etx
- Express has a router so routes can be stored in sperate files and export
- Incoming data can be parsed with the body parser.

```
app.get('/', (req, res) => {
    // Fetch from database
    // Load pages
    // Return JSON
    // Full access to request & response
});
```

### Express Middleware

- Middleware functions are functions that have access to the request and response object. Express has built in middleware.
- Execute any code
- Make changes to the request/response objects
- End response cycle
- Call next middleware in the stack
