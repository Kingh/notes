﻿# Java OOP (Object-Oriented Programming) Principles

> OOP definition

- OOP is a programming model in which programs are organized around data, or object, rather than functions and logic.

> Principles of OOP

- the four principles of object-oriented programming are:
  - Encapsulation
  - Abstractions
  - Inheritance
  - Polymorphism

---

#### Encapsulation

- Encapsulation is achieved when each object keeps its state private, inside a class.
- Other objects don't have direct access to the state, except through the class's setters and getters.
- The object manages its own state via methods, and no other class can alter the state unless explicitly allowed.

---

#### Abstraction

- Abstraction can be thought of as a natural extension of encapsulation.
- Abstraction is the process of hiding internal implementation details of an object and exposing a high-level mechanism for using it.

---

#### Inheritance

- Inheritance allows an object to acquire the the features of another class.
- Inheritance allows for the reuse of common logic between similar objects
- The inheriting class reuses all the public fields and methods from the inherited class and can implement its own unique parts.

---

#### Polymorphism

- Polymorphism is the ability for an object to take on many forms.
- **Compile time polymorphism**: also known as static polymorphism. This type of polymorphism is achieved through function overloading or operator overloading.
  - **Method Overloading**: When there are multiple functions with the same name but different implementation.
- **<color green> Runtime polymorphism**: also known as Dynamic Method Dispatch. It's a process by which a function call to the overridden method is resolved at runtime.
  - **<color green> Method overridding**: is when a derived class redefines one of the member functions of the base class.
