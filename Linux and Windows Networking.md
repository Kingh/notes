﻿# How to Share Files Between Windows and Linux

#### Create a shared folder on windows

1. Open the Control Panel
2. Go to **Network and Sharing Options**
3. Go to **Change Advanced Sharing Settings**
4. Select **Turn on Network Discovery** and **Turn on File and Print Sharing**

> Create a folder to share or choose an existing folder to share

1. Right-click the folder and select **Properties**
2. Go to the **Sharing** tab
3. Click on **Advanced Sharing** enable the **Share this folder** checkbox and click ok.
4. Click share

#### Access a Windows shared folder from Linux, using file manager (Nautilus)

1. Open the file manager
2. From the **File** menu select **Connect to Server**
3. In the **Server:** field, enter the name of the computer to connect to.
4. Supply the **User name** and **Password** used to log into the computer.
5. Click connect.  