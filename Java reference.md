# Primitive Types

---

### Integers

> Whole-value signed numbers

1. byte
2. short
3. int
4. long

### Floating-point numbers

> Numbers with fractional precision

1. float
2. double

### Characters

> Character set symbols i.e letters and numbers

1. char

### Boolean

> Type for representing true/false values

1. boolean

---

## Integer Types

---

- _in Java an int is always 32 bits, regardless of the particular platform_
- _Java does not support unsigned, positive only integers._

### byte

---

> The smallest integer type

- **_byte_** is a signed 8-bit type that has a range from -128 to 127.
- **_byte_** variables are useful for working with streams of data from a network or file.
- also useful when working with raw binary data that may not be directly compatible with Java's other built-in types.

> _byte declaration_:
> `byte b, c;`

### short

---

> The least used integer type in Java

- **_short_** is a signed 16-bit type, ranging from -32,768 to 32,767

> _short declaration_:

```
short s;
short t;
```

### int

> The commonly used integer type

- **_int_** is a signed 32-bit type ranging from -2,147,483,648 to 2,142,483,647.
- One common use of the **_int_** variable type is as controls for loops and to index arrays.
- When byte and short values are used in expressions they are promoted to an **_int_** when the expression is evaluated.

### long

> The largest integer type

- **_long_** is a signed 64-bit type. The range of long is quite large.
- **_long_** is useful in instances where an **_int_** type can't hold the desired value

---

## Floating-Point Types

- Floating-point numbers, also known as real numbers, are useful for computations that require fractional precision

- There are two kinds of floating types, **_float_** and **_double_**, which represent single- and double-precision numbers respectively.

### float

> The type **_float_** specifies a single-precision value

- **_float_** uses 32-bits of storage
- single precision is faster and takes half the space of double precision.
- but it is imprecise for very large and very small values.

> \_float declaration:
> `float hightemp, lowtemp;`

### double

> The type **_double_** specifies a double-precision value

- double-precision uses 64-bits to store a value.
- double-precision is faster than single precision on some modern processors optimized for high speed mathematical calculations.
- **_double_** is useful for maintaining accuracy over many iterative calculations and large-valued numbers.

---

## Characters

- the **_char_** stores characters in Java
- Java uses _Unicode_ to represent characters

### char

> In Java **_char_** is a 16-bit type

- **_char_** ranges from 0 to 65,536
- **_char_** have no negative values.
- **_char_** can also be used as an integer type on which to perform arithmetic operations

---

## Booleans

- Java has a primitive type, called **_boolean_**, for logical values
- **_boolean_** can have one of two values true or false.
- **_boolean_** is the type returned by all relational operators i.e. `a < b`.
- **_boolean_** is the type _required_ by the conditional expressions that govern control statements such as if and for.

---

## Integer Literals

- Integers are the most used type in programs, whole numbers values are integer literal i.e. 1, 2, 3 100. These are decimal (base 10) values.
- Other bases that can be used as integer literals are octal (base 8) and hexadecimal (base 16).
- Octal values are denoted in java by a leading zero, decimal numbers should not have a leading zero.
- Octal ranges from 0 to 7.
- Hexadecimal is a more common base used by programmers, and matches cleanly with modulo 8 word sizes i.e. 8, 16, 32, 64 bits.
- A hexadecimal constant is specified with a leading zero-x (0x or 0X).
- The range for a hexadecimal is 0 to 15, with 10 to 15 substituted by A to F or a to f.
- Integer literals create an int value, which in java is a 32-bit integer value. When a literal value is assigned to a byte or a short variable, no error is generated if the literal value is within the range of the target type.
- An integer literal can be assigned to a long variable by explicitly telling the compiler that the literal value is of type long, this is done by appending an upper- or lowercase L to the literal i.e. `0x7ffffffffffffffL` or 9223372036854775807L which is the largest long.
- An integer can also be assigned to a char as long as it is within range
- Integer literals can also be specified using binary by prefixing the value with 0b or 0B. For example to declare the decimal value 10 using a binary literal use: `int x = 0b1010`.
- The range for binary is 0 to 1.

---

## Floating-Point Literals

- Floating-point numbers represent decimal values with a fractional part.
- They can be represented in either standard notation or scientific notation
- Standard notation: _2.0_, _3.14159_ and _0.557_
- Scientific notation: _6.022E23_, _214159E-05_ and _2e+100_
- Floating-point literals in java default to double precision. To specify a float literal append an F or f to the constant. To specify a double literal append a D or d to the constant which is redundant
- Hexadecimal floating-point literal example: _0x12.2P2_
- In the hexadecimal floating-point literal the value after P, called the binary exponent, indicates the power-of-two by which the number is multiplied

---

## Boolean Literals

- Boolean values do not convert into any numerical representation. The true literal in java does not equal 1, nor does the false literal equal 0.

---

## Character Literal

- Characters are indices into the unicode character set.
- A character literal is represented in single quotes.

---

## String Literals

- String literal in java are specified by enclosing a sequence of characters between a pair of double quotes. example: `"Hello World"`.

---

## Variables

### The Scope and Lifetime of Variables

- A block begins with a curly brace and ends with a closing curly brace, a block defines a scope.
- A scope determines what objects are visible to other parts of the program. It also determine the lifetime of those objects.
- The two scopes defined by java are the class and method scopes.
- Variables declared within a scope are not visible outside that scope.
- Variables are created when their scope is entered, and destroyed when their scope is left.

---

## Java's Automatic Conversion

- Automatic type conversion will take place if the following condition are met:
  - The two types are compatible
  - The destination is larger than the source type
- When these two conditions are met, a widening conversion takes place.
- For widening conversions, the numeric types, including integer and floating point types, are compatible with each other.
- There are no automatic conversions from the numeric types to char or boolean.
- Java also performs an automatic type conversion when storing literal integer constants into variables of type byte, short, long or char.

---

## Casting Incompatible Types

- To create a conversion between two incompatible types (narrowing conversion), use a cast. A cast is simple an explicit type conversion i.e. _(targe-type)value_, target type specifies the desired type to convert the specified value to.
- When casting integer to byte if the integer's value is larger than the range of a byte, it will be reduce modulo (the remainder of an integer division by the) byte's range.

```
int a;
byte b;
//...
b = (byte) a;
```

- A different type of conversion occurs when a floating point value is assigned to an integer type: _truncation_, which is when the fractional component is lost since integers do not have a fractional component.

---

## The Type Promotion Rules

- Java automatically promotes each byte, short or char operand to int when evaluating an expression.
- If one operand is a long, the whole expression is promoted to long. If one operand is a float, the entire expression is promoted to float, if any of the operand is a double, the result is double.

---

## Arrays

- Use new to allocate an array, the type and number of elements to allocate must be specified. Elements in the array allocated by new will be initialized to zero (for numeric type), false (for boolean) or null (for reference types)

---

## Introducing Type Inference with Local Variables

- To use local variable type inference, the variable must be declared with var as
  the type name and it must include an initializer. The type for the variable will be inferred from its initializer i.e. `var avg = 10.0` declares a double.
- var can only be used to declare initialized variables. Only one variable can be declared at a time. A variable can not be initialized with null. var can not be used with array initializer i.e. `var myArray = {1, 3, 5} // wrong`.

---

## The Bitwise Operators

- Java defines _bitwise operators_ that apply to integer types. These operators act upon the individual bits of their operands

| Operator | Result                           |
| -------- | -------------------------------- |
| ~        | Bitwise unary NOT                |
| &        | Bitwise AND                      |
| \|       | Bitwise OR                       |
| ^        | Bitwise exclusive OR             |
| >>       | Shift right                      |
| >>>      | Shift right zero fill            |
| <<       | Shift left                       |
| &=       | Bitwise AND assignment           |
| \|=      | Bitwise OR assignment            |
| ^=       | Bitwise exclusive OR assignment  |
| >>=      | Shift right assignment           |
| >>>=     | Shift right zero fill assignment |
| <<=      | Shift left assignment            |

- All integers are represented by binary numbers of varying bit widths.
- Java uses two's complement encoding, which means that negative numbers are represented by inverting all the bits in th value, then adding 1 to the result i.e- 42 is represented by inverting all ot the bits in 42 [00101010], which yields 11010101, then adding 1, which results in 11010110 [-42].
- To decode a negative number, first invert all the bits, then add 1
- Computer languages use two's complement to avoid zero crossing, which is turning -1 into -0 which is invalid.
- Negative numbers are store using two's complement in Java
- The high order bit determines the sign of the integer no matter how that high-order bit get set, thus applying bitwise operators can produce unexpected results.

| A   | B   | A\|B | A&B | A^B | ~A  |
| --- | --- | ---- | --- | --- | --- |
| 0   | 0   | 0    | 0   | 0   | 1   |
| 1   | 0   | 1    | 0   | 1   | 0   |
| 0   | 1   | 1    | 0   | 1   | 1   |
| 1   | 1   | 1    | 1   | 0   | 0   |

> The table shows the outcome of bitwise logical operators operation on bits

### The Bitwise NOT

- Also called the _bitwise complement_, the unary NOT operator, ~. inverts all of the bits of its operand.

### Bitwise AND

- The AND operator, &, produces a 1 bit if both operands are 1, a zero in all other cases.

### The Bitwise OR

- The OR operator, |, combines bits such that if either of the bits in the operands is 1, then the resultant bit is a 1.

### The Bitwise XOR

- The XOR operator, ^, combines bits such that if exactly one operand is 1, the result is 1, otherwise the result is zero.

### The Left Shift

- The left shift operator , **<<**, shifts all of the bits in a value to the left a specified number of times it has this form: `value << num`. _num_ specifies the number of positions to left-shift _value_.
- For each shift left, the high-order bit is shifted out (and lost), and a zero is brought in on the right.
- When applying the left-shift on an int operand, the bits are lost once they are shifted past bit position 31, and after bit 63 for a long.
- Java's automatic type promotions produce unexpected results when shifting byte and short values, as byte and short are promoted to in when an expression is evaluated. And the result of the operation is an int.
- A negative byte or short value will be sign-extended when it is promoted to int, the high order bits will be filled with 1's.
- Each left shift has the effect of doubling the original value, the left-shift operator can be used as an efficient alternative to multiplying by 2, but caution should be applied as shifting a 1 bit into the high-order position, will cause the value to be negative.

### The Right Shift

- The right shift operator, **>>**, shifts all of the bits in a value to the right a specified number of times. It's form is: `value >> num`. _num_ specifies the number of positions to right-shift _value_.
- When bits of a value are "shifted off", those bits are lost.
- Shifting a value to the right has the same effect as diving the value by two---and discarding any reminder.
- When shifting right, the leftmost bits exposed by the right shift are filled with the previous contents of the top bit, this is called sign extension and serves to preserve the sign of negative numbers.

### The Unsigned Right Shift

- Unsigned shift is the shifting of a zero into the high-order bit no matter what its initial value was.
- Unsigned shifting is accomplished by Java's unsigned right shift operator, **>>>**

---

## Relational Operators

- Relational operators determine the relationshiop that one operand has to the other. They determine equality and ordering.

| Operator | Result                   |
| -------- | ------------------------ |
| ==       | Equal to                 |
| !=       | Not Equal to             |
| >        | Greater than             |
| <        | Less than                |
| >=       | Greater than or equal to |
| <=       | Less than or equal to    |

- The outcome of relational operators is a boolean value. This operators are used to control loops and if statements.
- Any type in Java can be compared using the **==** and not **!=**. The ordering operators can only be used on numeric types.
- In Java, **true** and **false** are nonnumeric.

---

## Boolean Logical Operators

| Operator | Result                   |
| -------- | ------------------------ |
| ==       | Equal to                 |
| !=       | Not Equal to             |
| >        | Greater than             |
| <        | Less than                |
| >=       | Greater than or equal to |
| <=       | Less than or equal to    |
| &        | Logical AND              |
| \|       | Logical OR               |
| ^        | Logical XOR              |
| \|\|     | Short-circuit OR         |
| &&       | Short-circuit AND        |
| !        | Logical unary NOT        |
| &=       | AND assignment           |
| \|=      | OR assignment            |
| ^=       | XOR assignment           |
| ==       | Equal to                 |
| !=       | Not Equal to             |
| ?:       | Ternary if-then-else     |

- The logical Boolean operator &, | and ^ operate on boolean values in the same way they operate on the bits of an integer.

### Short-Circuit Logical Operators

- These operators are the secondary versions of the Boolean AND and OR.
- Using || and && forms, rather than the | and & form of the operators, Java will not bother to evaluate the right-hand operand when the outcome of the expression can be determined by the left-hand operand alone. This is useful when the right-hand operand depends on the value of the left one in order to function properly.

### The ? Operator

- The ternary operator can replace certain types of if-then-else statements.
- It has this general form: `expression1 ? expression2 : expression3`. Here, _expression1_ can be any expression that evaluates to boolean. If _expression1_ is true, the _expression2_ is evaluated; otherwise _expression3_ is evalueated.

---

## Operator Precedence

| Highest     |             |     |     |           |          |             |
| ----------- | ----------- | --- | --- | --------- | -------- | ----------- |
| ++(postfix) | --(postfix) |     |     |           |          |             |
| ++(prefix)  | ++(prefix)  | ~   | !   | +(unary)  | -(unary) | (type-cast) |
| \*          | /           | %   |     |           |          |             |
| +           | -           |     |     |           |          |             |
| >>          | >>>         | <<  |     |           |          |             |
| >           | >=          | <   | <=  | instaceof |          |             |
| ==          | !=          |     |     |           |          |             |
| &           |             |     |     |           |          |             |
| ^           |             |     |     |           |          |             |
|             |             |     |     |           |          |             |  |
| &&          |             |     |     |           |          |             |
|             |             |     |     |           |          |             |  |  |
| ?:          |             |     |     |           |          |             |
| ->          |             |     |     |           |          |             |
| =           | op=         |     |     |           |          |             |
| Lowest      |             |     |     |           |          |             |

- The table shows operator precedence from highest to lowest. Operators in the same column have the same precedence, and are evaluated left to right.

### Using Parentheses

- Parentheses raise the precedence of the operations that are inside them, they can also be used to clarify an expression.

---

# Control Statements

- Programming languages use control statements to control the flow of execution based on changes to the state of the program.
- Java's program control statements can be put into the following categories:
  - selection
  - iteration
  - and jump
- _Selection_ statement allow a program to choose different paths of execution based on the outcome of an expression or the state of a variable.
- _Iteration_ statement enable program execution to repeat one or more times (iteration statements form loops).
- _Jump_ statements allow a program to execute nonlinearly.

## Java's Selection Statements

- Java supports two selection statements: if and switch. These statements control the flow of a program based on conditions known only during run time.

### if

- The if statement is Java's conditional branch statement. The if statement can be used to route a program execution through different paths. **if** general form:

```
if (condition) {
  statement1;
} else  {
  statement2;
}
```

- The _condition_ must be a value that returns a boolean. The **else** clause is optional.
- If the _codition_ is true, the _statement1_ is executed, otherwise _statement2_ if it exists is executed.

### switch

- The switch statement is Java's multiway branch statement. It provides a way to dispatch execution to different parts of the code based on the value of the expression. It is often a better alternative than a large series of **if-else-if** statement. **switch** general form:

```
switch (expression) {
  case value1:
    //statement sequence
    break;
  case value2:
    //statement sequence
    break;
    .
    .
    .
    .
  case valueN:
    //statement sequence
    break;
  default:
    //default statement sequence

}
```

- _expression_ must resolve to type **byte**, **short**, **int**, **char**, **enumeration** or **String**.
- Each value specified in the **case** statement must be a unique constant expression (such as a literal value). Duplicate **case** values are not allowed, the type of each value must be compatible with the type of _expression_.
- The value of _expression_ in **switch** is compared with each of the values in the case statement, if a match is found the code sequence following the **case** statement is executed, if no match is found then the default statement is executed, else if the default statement is absent no action is taken. The default statement is optional.
- The _break_ statement is used to terminate a statement sequence. When a _break_ is encountered execution branches out of the **switch** statement.
- Switching on string can be expensive than switching on strings.
- The **switch** differs from the **if** in that a **switch** tests for equality, whereas **if** evaluates any type of boolean expression.
- A **switch** statement is usually more efficient than a set of nested **if**s.
- When the Java compiler compiles a **switch** statement, it will inspect each of the **case** constants and create "jump table", that it will use for selecting the path of execution depending on the value of the _expression_.

## Iteration Statements

- Java's iteration statements are:
  - **for**
  - **while**
  - **do-while**
- These statements create _loops_

### while

- The **while** loop is Java's most fundamental loop statement. It repeats a block while its controlling expression is true.

```
while (condition) {
  // body of loop
}
```

- The condition is any Boolean expression.
- The loop repeats until the _condition_ is false, when _condition_ is false control passes to the next line of code following the loop.

### do-while

- The **do-while** loop is useful in cases where a loop needs to execute once even if the _condition_ is false, since a **while** loop will not execute even a single time if the _condition_ is false.

```
do {
  // body of loop
} while (condition);
```

- Each iteration of the **do-while** loop first executes the body of the loop and then evaluates the _condition_. If the _condition_ is true the loop repeats, otherwise the loop terminates.

### for

- There are two forms of the **for** loop, the first is the traditional form the second is the "for each".

```
// traditional for
for (initializtion; condition; iteration) {
  // body
}

// for-each
for (type value: values) {
  // body
}
```

- _initialization_ is executed at the start of the loop, this is generally an expression that sets the value of the loop variable, the loop variable is normally a counter, this variable is set only once.
- _condition_ is evaluated next, it be a boolean expression. The loop will continue execution until the condition is false. This part is evaluated at each iteration of the loop.
- _iteration_ this part is used to increment the counter and is executed after the condition is evaluated. After the iteration the body of the loop is executed.

### for Loop Variations

- The three parts of the _for_ loop can be used for any purpose.
- The _condtion_ does not need to test the loop control variable against a target value.
  The condition can be any boolean expression. For example:

```
boolean done = false;

for (int i = 1; !done; i++) {
  // ...
  if (interrupted()) {
   done = true;
  }
}
```

- Leaving all three parts of the _for_ loop empty creates an infinite loop:

```
for(;;) {
  // ...
}
```

### The For-Each Version of the for Loop

- A second form of for implements a “for-each” style loop. A _for-each_ loop cycles through a collection of objects, such as an array, sequentially from start to finish.
- In Java the _for-each_ loop is also referred as _enhance for_ loop. for-each general form:

```
for (type itr-var: collection) {
  statement-block;
}
```

- _type_ specifies the type and it-var specifies the iteration variable that receives elements from the collection, one at a time. the type for _type_ must match _collection_ type. -_collection_ specifies the collection being cycled through.
- With each iteration of the loop, the next element is retrieved from _collection_ and stored in _itr-var_. The loop repeats until all elements have been obtained from _collection_.

### Local Variable Type Inference in a for Loop

- Local variable type inference can be used in _for_ loops, when declaring the loop control variable in a for or when specifying the iteration variable in a _for-each_ loop.

```
....
for (var x = 2.4; condition; iteration) {
  ....
}

for (var v; nums) {
  ....
}
```

---

## Jump Statement

- Java supports three jump statements:
  - **break**
  - **continue**
  - **return**

### Using break

- In Java, the break statement has three uses:
  - It terminates a sequence in a **switch** statement.
  - It can be used to exit a loop.
  - It can also be a "civilized" form of goto.

#### Using break to Exit a Loop

- Using **break** inside a loop, forces immediate termination of a loop, bypassing the conditional expression and any remaining code in the body of the loop. This causes the program to resume at the next statement after the loop.
- The **break** statement can be used to exit an infinite loop.
- When used inside a nested loop, the break statement will only break out of the innermost loop.

#### Using break as a form of Goto

- Java doesn't support goto statements as they provide an unstructured and arbitrary way of branching out of code. This makes goto code hard to maintain and understand.
- There are cases where goto is a valuable and legitimate construct for flow control. For instance goto is useful for exiting out of nested loops. To handle such cases Java defines an expanded form of the **break** statement.
- This form of **break** statement works with labels to exit deeply nested loops or switch statements.
- This also allows the specifying where execution will resume as it works with labels.

```
break label;
```

- Label is the name of a label that identifies a block of code. When this form of **break** executes, control is transferred out of the named block.
- The labeled block must enclose the **break** statement, but doesn't necessarily need to be immediately enclosing the block.
- To name a block, put a label at the start of it. A _label_ is any valid Java identifier followed by a colon. Once labeled use a **break** statement to target the label.

```
outer: for (int i = 0; i < 3; i++) {
  for (int j = 0; j < 100; j++) {
    if (j == 10) {
      break outer;
    }
  }
}
```

### Using continue

- **continue** is useful for forcing an early iteration of a loop, it stops the processing of the entire loop for a particular iteration. This is similar to a goto as it bypasses the body of the loop, to the loops end.
- In a **while** and **do-while** loops, a **continue** statement causes control to be directly transferred to the conditional expression controlling the loop.
- In a **for** loop, control to the iteration portion of the **for** and then to the conditional expression.
- For all three loops, any intermediate code is bypassed.
- Similar to a **break** statement, continue may specify a label to describe which enclosing loop to continue.

```
outer: for (int i = 0; i < 3; i++) {
  for (int j = 0; j < 100; j++) {
    if (j == 10) {
      continue outer;
    }
  }
}
```

### return

- The **return** statement is used to explicitly return from a method. It causes program control to transfer back to the caller method.
- The return statement immediately terminates the method in which it is executed.

---

## Classes

- The class is the core of Java. Any implemented concept in a Java program must be encapsulated within a class.

### Class Fundamentals

- A class defines a new data type, once created this new type can be used to create object of that type.
- A class is a template for an object, and an object is a runtime entity or an instance of a class.

### The General Form of a Class

- When defining a class, its exact form and nature are declared. This is done by specifying the data it contains and the methods that operate on the data.
- A class' code defines the interface to its data.
- A class is declared with the **class** keyword.
- A simplified general form of a class definition:

```
class classname {
  type instance-variable1;
  type instance-variable2;
  // ...
  type instance-variableN;

  type methodname1(parameter-list) {
    //body of method
  }

  type methodname2(parameter-list) {
    //body of method
  }

  // ...
  type methodnameN(parameter-list) {
    //body of method
  }
}
```

- Data or variables defined within a class are called instance variables. Method contain the class' code. A class' data and method are collectively called member of that class.
- The instance variables are acted upon and accessed by the methods defined for that class in most classes. Methods determine how a class' data can be used.
- Each instance of a class containes its own copy of the instance variables. Data from one object is separete and unique from the next object's data.

### Declaring Objects

- Obtaining object of a class is a two-step process:
  - First, declare a variable of the class type, this variable doesn't define an object, it can refer to an object.
  - Second, create a new object of the class by using the **new** operator and assign the object to the variable.
- The **new** operator dynamically allocates (allocates at run time) memory for an object and returns a reference to it. The reference is the address of the memory where the object is located, this reference is stored in the variable.

### new Operator

- As mentioned above the **new** operator dynamically allocates memory for an object. It has this general form [context: assignment]: `class-var = new classname();`
- _class-var_ is a variable of the class type being created. The _classname()_ is the constructor of the class being instantiated.

### Assigning Object Reference Variables

- When assigning a class variable from an existing variable which references an object, the two variable will reference the same object instead of a copy being created and assigned to the new variable.

```
Object ob1 = new Object();
Object ob2 = ob1;
```

- After the above code fragment executes ob1 and ob2 will refer to the same object. The assignment did not allocate any memory or copy any par of the original object.
- If a change is made through ob2, this will affect the object referred by ob1 since they are the same object.
- A reassignment will simply unhook a reference without affecting the remaining reference and object since the references are not linked in any other way.

### Class Methods

- Classes usually consist of two things: instance variables and methods.
- Method general form:

```
type name(parameter-list) {
  // body of method
}
```

- _name_ specifies the method name, this should be any legal identifier and _type_ before name specifies the return type of the method. _type_ can be any valid type, including created class types, if the method returns no value, the return type must be **void**.
- The _parameter-list_ is a sequence of type and identifiers pairs seprated by commas. Parameters are variables that receive the value of the arguments passed to the method when it is called. A method without

### Parameters and Arguments

- A _parameter_ is a variable defined by a method that receives a value when the method is called. An arguments is a value that is passed to a method when it is invoked.

### Constructors

- A _constructor_ is a method used to initialize an object on creation. It has the same name as the class it's defined in and no return type because the implicit return type is the class type itself.
- Constructors are automatically called when the object is created, before the new operator completes.
- When a _constructor_ is not defined in a class Java creates a default parameter-less constructor for the class.
- When using a default _constructor_, all non-initialized instance variables are set to their default values; which are zero, **null** and **false**, for numeric types, reference types and **boolean** respectively.

### The this Keyword

- The **this** keyword is commonly used inside any method to refer to the _current_ object.

### Instance Variable Hiding

- Local variables and method parameters can have names overlapping with a class' instance variables. When a local variable has the same name as an instance variable, the local variable hides the instance variable.
- The **this** keyword helps resolve any namespace collision that might occur between instance variables and local variables:

```
private type var1;
private type var2;
private type var3;

constructor(type var1, type var2, type var3) {
  this.var1 = var1;
  this.var2 = var2;
  this.var3 = var3;
}
```

### Garbage Collection

- Java handles memory deallocation automatically through a technique called _garbage collection_.
- I works like this; when no reference to an object exist, the object is assumed to be no longer needed, and the memory occupied by the object can be reclaimed. Garbage collection occurs sporadically (if at all) during the execution of the program.

---

### Overloading Methods

- In Java, it is possible to define two or more methods within the same class that share the same name, as long as their parameter declarations are different. This is known as _method overloading_.
- _Method overloading_ is one of the ways that Java supports polymorphism.
- When an overloaded method is invoked, Java uses the type and or number of arguments as its guide to determine which version of the overloaded method to actually call. Thus, overloaded methods must differ in the type or number of parameters.
- For overloaded methods with differing return type, the return type alone is insufficient to distinguish two versions of a method. Return types play no role in overload resolution.

```
....
void test() {

}

void test(int a) {

}

void test(int a, int b) {

}
....
```

- In some cases, Java's automatic type conversions can play a role in overload resolution. Automatic type conversion will be employed if no exact match is found.

```
....
void test() {

}

void test(double a) {

}
....
int i = 88;

obj.test(i); // this will invoke test(double)
```

- Method overloading supports polymorphism because it is one way that Java implements the "one interface, multiple methods" paradigm.

### Overloading Constructors

- Similar to methods constructors can be overloaded as well.

### Argument Passing

- There are two ways to pass arguments to a subroutine.
  - The first way is _call-by-value_, this approach copies the value of an argument into the formal parameter of the subroutine, meaning, changes made to the parameter of the subroutine have no effect on the argument.
  - The second way to pass an argument is _call-by-reference_. In this approach, a reference to an argument (not the value of the argument) is passed a to the parameter. Inside the subroutine, this reference is used to access the actual argument specified in the call. Meaning that changes made to the parameter will affect the argument used to call the subroutine
- In Java, primitive types are passed by value to a method.
- And object are passed by reference, thus changes to the object inside the method affect the object used as an argument.

### Access Control

- Encapsulation links data with the code that manipulates it, and also provides _access control_ to the data.
- How members of a class can be accessed is determined by the _access modifier_ attached to its declaration.
- Java's access modifiers are **public**, **private**, **protected** and the default access level. **protected** applies when inheritance is involved.

### static

- A **static** method is one that can be accessed without a reference object.
- **static** method restrictions:
  - Can only directly call other static methods of their class.
  - Can only directly access static variables of their class.
  - Cannot refer to this or super in any way.
- Instance variables declared as static are global variables. When objects of a class are declared , no copy of the static variable is made, all object of the class will share the same static variable.

### final

- A field can be declared **final**, this prevents the field from being modified, making it a constant. If a field is declared **final** it must be initialized during declaration or inside a constructor.
- Methods and method parameters can also be declared **final**. Declaring a parameter final prevents its modification inside the method. And declaring a local variable **final** prevents it form being assigned a value more than once.

### Arrays Revisited

- Arrays are implemented as objects in Java.

---

## Nested and Inner Classes

- Nested classes are classes defined within other classes
- The scope of a nested class is bounded by the scope of the enclosing class, thus a nested class does not exist independently of the enclosing class.
- A nested class has access to the members of the enclosing class, including the private methods, however the enclosing class does not have access to the members of the nested class.
- A nested class declared directly within its enclosing class scope is a member of the enclosing class.
- Nested classes can be declare withing any blocks
- There are two types of nested classes:

  - _static_ - static nested classes have the **static** modifier applied. Static nested classes must access the non-static members of the enclosing class through an object.
  - _non-static_ / _inner-class_ - inner-classes have access to all the variables and methods of the enclosing class and may refer to them directly.

- An _inner-class_ can only be instantiated within its enclosing class

```
class Outer {
  var outer_x = 100;

  void test() {
    Inner inner = new Inner();
    inner.display();
  }

  // Inner class
  class Inner {
    void display() {

    }
  }
}
```

---

## Inheritance

- Inheritance allows the creation of hierarchical classification.
- With inheritance a general class that defines traits common to a set of related items can be created. This class can then be inherited by other, more specific classes, each adding only traits unique to it. An inherited class is called a _superclass_ and an inheriting class is call a _subclass_ in Java. A subclass is a specialized version of a superclass.

### Inheritance Basics

- general inheritance declaration template:

```
class subclass-name extends superclass-name {
   ....
}
```

- Only one superclass can be inherited.

### Superclass Reference

- A superclass reference variable can be used to reference a subclass object.
- **NOTE**: It is important to understand that it is the type of the reference variable, not the object type, that determines what members can be accessed. When a subclass object is referenced by a superclass variable, only those parts of the objects defined by the superclass can be accessed.

### Dynamic Method Dispatch

- Dynamic method dispatch is the mechanism by which a call to an overridden method is resolved at run time, rather than compile time, which is how Java implements run-time polymorphism.
- Since a superclass reference variable can refer to a subclass object, Java uses this to resolve calls to overridden methods at run time. Java determines which version of a method to call based upon the type of object being referred to at the time of the method call, _it is the type of the object being referred to not the reference that determines which version of an overridden method will be executed_.

### Why Overridden Methods?

- Overridden methods allow Java to support run-time polymorphism.
- Polymorphism is essential to object-oriented programming for the reason that it allows a general class to specify methods that will be common to all of its derivatives, while allowing subclass to define the specific implementation of some or all of those methods.
- Overridden methods are another way for Java to implement the "one interface, multiple methods" aspect of polymorphism.

### Using Abstract Classes

- Abstract classes or methods are implemented in situation where a superclass is needed to define the structure of a given abstraction without providing complete implementation of every method.
- To declare an abstract method use the general form: _abstract type name(parameter-list);_
- A class with abstract method must also be declared abstract. Abstract classes can not be directly instantiated.
- All subclasses of an abstract class must implement all abstract method of the abstract superclass or be declared abstract as well.

### Using final with Inheritance

- The final keyword can be used to avoid method overriding: _final type methodName(parameter-list)_
- Late binding is the process where Java resolves calls to methods dynamically at runtime. When a small final method is called, the Java compiler can copy the bytecode for the subroutine directly inline with the compiled code, this is called early binding.
- final can also be used to prevent a class from being inherited. final classes can not be abstract as all the methods are implicitly final:

```
final class A {
  //All method withing implicitly final.
}
```

### Local Variable Type Inference and Inheritance

- When working with inheritance, the inferred type is the declared type of the initializer.

---

## The Object Class

- The **Object** class is defined by Java, and all classes are subclasses of this class.
- The reference variable of type **Object** can refer to an object of any class.
- The following method are defined in the **Object** class and are available to all objects:

| method                                             | Purpose                                                         |
| -------------------------------------------------- | --------------------------------------------------------------- |
| Object clone                                       | Create a new object that is the same as the object being cloned |
| boolean equals(Object object)                      | Determines wether one object is equal to another                |
| void finalize()                                    | Called before an unused object is recycled (Deprecated by JDK9) |
| final Class<?> getClass()                          | Obtains the class of an object at run time                      |
| int hashCode()                                     | Returns the hash code associated with the invoking object       |
| final void notify                                  | Resumes execution of a thread waiting on the invoking object    |
| final void notifyAll()                             | Resumes execution of all thread waiting on the invoking object  |
| String toString()                                  | Returns a string that describes the object.                     |
| final void wait                                    | Wait on another thread of execution                             |
| final void wait(long millisecond)                  |                                                                 |
| final void wait(long millisecond, int nanoseconds) |                                                                 |

---

## Packages and Interfaces

- Packages are containers for classes, used to keep the class name space compartmentalized.
- **interface** specify a set of methods that can be implemented by classes. The **interface** does not define any implementation except for a default implementation since (JDK)
- Interfaces are similar to abstract class except that a class can implement more than one interface.

### Packages

- A package is both a naming and a visibility control mechanism. Classes can be defined that are only accessible by code within the package.

#### Finding Packages and CLASSPATH

- Packages are mirrored by directories.
- How the Java run-time system finds where created packages are:
  - By default Java run-time system uses the current working directory as the starting point, if the package is in a subdirectory of the current directory, it will be found.
  - The directory path can be specified by setting the **CLASSPATH** env variable.
  - Third **-classpath** option can be used during program execution and compilation.

#### Packages and Member Access

- Classes and packages are means of encapsulating and containing the name space and scope of variables and methods. Packages act as containers for classes and subordinate packages.

> **Table 9-1** Class Member Access

|                                | Private | No Modifier | Protected | Public |
| ------------------------------ | ------- | ----------- | --------- | ------ |
| Same class                     | Yes     | Yes         | Yes       | Yes    |
| Same package subclass          | No      | Yes         | Yes       | Yes    |
| Same package non-subclass      | No      | Yes         | Yes       | Yes    |
| Different package subclass     | No      | No          | Yes       | Yes    |
| Different package non-subclass | No      | No          | No        | Yes    |

- A non-nested class can only be default or public. A default class is only be access by other classes withing the same package.

### Interfaces

- Interfaces are used to fully abstract a class' interface from its implementation.
- Interfaces are declared with the **interface** keyword.
- Interface definition:

```
access interface name {
  type final-varname = value;
  type final-varname = value;

  return-type method-name1(parameter-list);
  return-type method-name2(parameter-list);
}
```

- Interfaces without an access modifier are only visible to other member of the same package.
- Variables in interfaces are implicitly **final** and **static**
- A class that implements an interface but does not implement the interface's method must be declared abstract.

#### Nested Interfaces

- _Member or nested interfaces_ are interfaces declared as members of a class or another interface.
- Nested interfaces can be public, private or protected.
- Nested interface are qualified with the name of the enclosing class or interface when used outside the enclosing scope.

#### Multiple Inheritance Issues

- Multiple inheritance rules:

  - A class implementation takes priority over and interface default implementation.
  - An error will result if a class implements two interface with similar default methods and does not override the method.
  - If an interface extends another interface with a similar method the inheriting interface's method take precedence.

---

# Exception Handling

- An _exception_ is an abnormal condition that arises in a code sequence at run time, an exception is a run-time error.

## Exception Types

- All exceptions types are subclasses of the built-in class **Throwable**.
- **Exception** and **Error** are subclasses that are immediately below **Throwable**.
- **RuntimeException** is a subclass of **Exception** that is automatically defined to handle errors such as division by zero and invalid array indexing.
- **Error** is defined for handling Java run-time environment exceptions.

> to be continued

---

# Multithreaded Programming

- Multithreading is a specialized from of multitasking. A multithreaded program contains two or more parts that can run concurrently.
- There are two types of multitasking: _process-based_ and _thread-based_.
- Process-based multitasking enables computers to run two or more programs concurrently.
- A thread in _thread-based_ multitasking environment is the smallest unit of dispatchable code.

## The Java Thread Model

- Single-threaded systems use an approach called an _event loop_ with _polling_, in this model a single thread of control run in a infinite loop, polling a single event queue to decide what to do next. Until an event handler return, the program remains idle.
- A thread exists in several states:
  - _running_
  - _ready to run_
  - _suspended_
  - _resumed_
  - _blocked_

## Thread Priorities

- Each Java thread is assigned an integer to specify the relative priority with respect to other threads. A thread's priority is used to decide when to switch from one running thread to the next, this is called _context switching_.
- Context switch rules:
  - A _thread can voluntarily relinquish control_ by yielding, sleeping or when blocked. In this scenario, other thread are examined and the highest-priority, ready to run thread runs.
  - A _thread can be preempted by a higher-priority thread_. A lower-priority thread that does not yield the processor is simply interrupted by a higher-priority thread as soon as the higher-priority thread wants to run, this is called preemptive multitasking.

## Synchronization

- The _monitor_ is a control mechanism that holds only one thread. Once a thread enters a monitor, all other threads must wait until that thread exits the monitor. Monitors help with interprocess synchronization by insuring that shared assets a manipulated by one thread at a time.
- Java uses synchronized method since no class "Monitor" exists in Java.

## Messaging

- Java has method define in the Object class for thread communication. Java's messaging system allows a thread to enter a synchronized method on an object, and wait till another thread notifies it to come out.

## The Thread Class and the Runnable Interface

- Java's multithreading system is build upon the **Thread** class and the **Runnable** interface. **Thread** encapsulated a thread execution.
- Threads are created through extending the **Thread** class or implementing **Runnable** interface.

| Method      | Meaning                                  |
| ----------- | ---------------------------------------- |
| getName     | Obtain a thread's name                   |
| getPriority | Obtain a thread's priority               |
| isAlive     | Determine if a thread is still running   |
| join        | Wait for a thread to terminate           |
| run         | Entry point for the thread               |
| sleep       | Suspend a thread for a period of time    |
| start       | Start a thread by calling its run method |
