﻿# Interview questions - Level 1

### Question: List the Object class's methods

> Answer:

- toString
- getClass
- wait
- notify
- notifyAll
- equals
- hashmap
- clone
- finalize
______________________________________________________

### Question: Why do we need the equals & hashCode methods?

> Answer:

- The equal method is used to compare equality of two objects.
- Objects can be compared in two ways:
	- Shallow comparison: the default implementation method defined in Java.lang.Object
 

