# Introduction To Higher Mathematics

## Lecture 1: Problem Solving 101

> What is a problem?

- A problem engages a student intellect.
- Stimulates students to make connections and develop coherent framework for mathematical reasoning.
- Are based on knowledge of the range of ways that diverse students learn mathematics.
- Good problem should foster students ability to reason and communicate mathematically.

### Phases of Problem Solving

1. Entry - Phase for thinking about the problem, ask this question:
   1.1 What do I **KNOW**?
   1.1.1 The above question is answered by reading the question.
   1.1.2 Also pondering what is known from past experience.
   1.2 What do I **WANT**?
   1.2.1 Write the problem in own words
   1.2.2 Watch out for possible ambiguities.
   1.3 What can I **INTRODUCE**?
   1.3.1 Make a diagram to better understand the question
   1.3.2 Write a mathematical notation.
2. Attack
   2.1 Brute force - try every single possibility to solve the problem
   2.2 Clever strategy - looking for a pattern
3. Review
   3.1 check the solution - make sure the solution solves the problem,
   3.2 Reflect on key ideas that lead to the solution - what were the big realization?
   3.2 Extend the solution to a wider context. More important than noticing a pattern, is understanding why it works.

---

## Lecture 2: Introduction to Proofs

> A proof is a demonstration that if some fundamental statements are assumed to be true, then some mathematical statement is necessarily true.

### Axiom

- The first step in writing proofs is to examine definitions, or if the problem calls for it make up definitions.
- A definition is an accurate description of a concept.
- Definitions require no proof whatsoever.
- Definitions should be reversible.
- After the definition, make initial assumption relative to the definitions to form axioms.
- An axiom (or postulate) is a statement that is accepted without proof.
- Everything to be proved must then be built off these axioms.
- Make as few assumption as possible.

### Theorems

- Using axioms, helps in proving theorems. With theorems, more theorems can be proven.

### Lemmas

- Sometimes a smaller result must be shown, before proving the main problem.
- First prove the mini-theorem called a lemma.
- A lemma is a stepping stone to the main result.

### Corollaries

- Once a theorem is proven, several result may easily follow from it these results are called corollaries.

### Conjectures

- A conjecture is an idea believed to be true but not proven.
- Some theorems are tentatively built on conjectures.

---

## Lecture 3: Propositional Logic

- A proposition is a statement that has one and only one of the truth values True (T) and False (F).
- Propositional logic variables: _p q r_.
- Propositional logic connectives _~ ∧ ∨ → ↔ ¬ ⊻_

| p   | q   | p ∧ q |
| --- | --- | ----- |
| F   | F   | F     |
| F   | T   | F     |
| T   | F   | F     |
| T   | T   | T     |

fig1: conjunction table.

| p   | q   | p ∨ q |
| --- | --- | ----- |
| F   | F   | F     |
| F   | T   | T     |
| T   | F   | T     |
| T   | T   | T     |

fig2: disjunction table.

| p   | q   | p ⊻ q |
| --- | --- | ----- |
| F   | F   | F     |
| F   | T   | T     |
| T   | F   | T     |
| T   | T   | F     |

fig3: Exclusive disjunction table.

| p   | ¬p  |
| --- | --- |
| F   | T   |
| T   | F   |

### Propositional Expressions

- Propositions can be linked with connectives to create propositional expression. Brackets can also be use to group expressions.

#### Order of Operations

1. Parentheses: () [] take highest precedence
2. ¬
3. ∧, left to right
4. ∨, left to right
5. →
6. ↔

#### De Morgan's Laws (special negations)

- _¬(p ∧ q) = ¬p ∨ ¬q_
- _¬(p ∨ q) = ¬p ∧ ¬q_

| P   | q   | p ∧ q | ¬(p ∧ q) | ¬p ∨ ¬q |
| --- | --- | ----- | -------- | ------- |
| F   | F   | F     | T        | T       |
| F   | T   | F     | T        | T       |
| T   | F   | F     | T        | T       |
| T   | T   | T     | F        | F       |

| P   | q   | p ∧ q | ¬(p ∨ q) | ¬p ∧ ¬q |
| --- | --- | ----- | -------- | ------- |
| F   | F   | F     | T        | T       |
| F   | T   | F     | F        | F       |
| T   | F   | F     | F        | F       |
| T   | T   | T     | F        | F       |

### Propositional Connectives

#### Conditional connective

- _p → q_, "if p then q" or "p implies q"
- p antecedent, q consequent

| P   | q   | p → q |
| --- | --- | ----- |
| F   | F   | T     |
| F   | T   | T     |
| T   | F   | F     |
| T   | T   | T     |

- conditional _(p → q)_
  - > If we win the game, then coach will take us for pizza
- converse _(q → p)_. To get the converse switch the conditional operands
  - > if the coach takes us for pizza, we must have won the game. Not equivalent to the conditional
- Inverse _(¬p ∧ ¬q)_
  - > if we don't win the game, then coach won't take us for pizza. Not equivalent to the conditional.
- Contrapositive _(¬q ∧ ¬p)_
  - > if coach didn't take us for pizza, then we must not have won the game. Equivalent to the conditional

#### Biconditional Connective

- _p ↔ q_, "p if and only if q" or "p iff q"
- same as _(p → q) ∧ (q → p)_
- also same as _(p → q) ∧ (¬p ∧ ¬q)_

| P   | q   | p ↔ q |
| --- | --- | ----- |
| F   | F   | T     |
| F   | T   | F     |
| T   | F   | F     |
| T   | T   | T     |

### Logical Equivalence

- The logical equivalence (⇔, "is equivalent to") shows a relationship between expressions.

---

## Lecture 4: Proof Techniques

> **NOTE:** proofs should be written in complete sentences rather than just collection of symbols

### Arguments

- An argument is an attempt to persuade someone of something, by giving reasons or evidence for accepting a particular conclusion.
- An argument has to parts:
  - The premise
  - and the conclusion
- For an argument to be considered valid the conclusion must follow from the premises.

### Modus Ponens

- Valid argument are based on a few basic forms, the first basic forms is _Modus Ponens (aka "law of detachment" or "affirming the antecedent")_.
- _Modus Ponens_ can be stated in the following way:

```
p → q  if p, then q

p
---  p. Therefore q
∴ q

let p = "I pass my logic test".
let q = "I am happy".

p → q  if i pass my logic test, then I will be happy

p
---  I passed my logic test
∴ q  Therefore, I am happy
```

| p   | q   | p → q | (p → q) ^ p |
| --- | --- | ----- | ----------- |
| F   | F   | T     | F           |
| F   | T   | T     | F           |
| T   | F   | F     | F           |
| T   | T   | T     | T           |

> Look at the last line to check if the argument is valid, since both premises are true the argument is true.

```
[(p → q) ∧ p] → q
```

| p   | q   | p → q | (p → q) ^ p | [(p → q) ∧ p] → q |
| --- | --- | ----- | ----------- | ----------------- |
| F   | F   | T     | F           | T                 |
| F   | T   | T     | F           | T                 |
| T   | F   | F     | F           | T                 |
| T   | T   | T     | T           | T                 |

> Tautology. If it can be proven that something is always true, then a tautology has be provent i.e the last column.

### Modus Tollens

- Another argument form is called _Modus Tollens or denying the consequent_
- _Modus Tollens_ can be stated in the following way:

```
p → q  if p, then q
¬q     Not q.
---
∴ ¬p   Therefore, not p

let p = "I pass my logic test".
let q = "I am happy".

p → q  if i pass my logic test, then I will be happy
¬q     I am not happy.
---
∴ ¬p   Therefore, I did not pass my logic test.
```

### Law of Syllogism

- The _Law of Syllogism_ allows for the chaining of a number of conditionals to form one large conditionals

```
p → q  if p, then q
q → r  if q, then r
---
∴ p → r  Therefore, if p, then r

p → q  if she weighs the same as a duck, then she's made of wood.
q → r  if she's made of wood, then she's a witch
---
∴ p → r  Therefore, if she weighs the same as a duck, then she's a witch.
```

### Affirming the Consequent

> NOTE DON'T DO THIS. This is a Fallacy

```
p → q  if p, then q
q      q.
---
∴ p    Therefore, p.
```

- This attempts to conclude the converse of the original condition. While a statement is equivalent to its contrapositive, it is not equivalent to its inverse or converse.

---

## Proof Techniques

- The most straight forward way is to assume the hypothesis and attempt to follow some mathematical steps and prove that the conclusion follows from it. This is called a direct proof.
- Goal: `p → q`
- Technique:
  - Assume _p_
  - Then prove _q_

### Direct Proof

> Theorem: if _a_ and _b_ are consecutive integers, then the sum _a + b_ is odd.

- To prove: `a + b` is odd.

```
b = a + 1
a + b = a + (a + 1)
      = 2a + 1
Even: 2n (n is some integer)
odd: 2n + 1 (an odd number is one more than the even number)
∴ 2a + 1 is odd, as this equals the original expression a + b must be odd.
```

Proof: Assume that `a` and `b` are consecutive integers. This means that `b = a + 1`.
Therefore the sum `a + b` may be rewritten as `2a + 1`, which by definition is odd. Thus `a + b` is odd. □

### Proof by Contrapositive

- Goal: `p → q`
- New Goal: `¬q ∧ ¬p`
- Technique:
  - Assume _¬q_
  - Then prove _¬p_

> Theorem: For any integer x, if x^2 is odd then x is odd. Contrapositive: for any integer x, if x is even then x^2 is even.

- To prove: if x is even then x^2 is even

```
x = 2n
x^2 = (2n)^2
    = 4n^2
    = 2 x 2n^2     let m = 2n^2
    = 2m
∴ x^2 is even as well
```

proof: Suppose x is even. This means it is equal to 2n for some integer n. So, `x^2 = 4n^2 = 2(2n^2)`. `let m = 2n^2`, in which case `x^2 = 2m`. Thus `x^2` is even as well. □

### Proof using Axiom

- Goal:_q_
- Technique:
  - Find axiom _p_
  - prove `p → q`

> Theorem: For any integer a, 0 \* a = 0.

```
0 + 0 = 0                                  Additive identity property (+)
(0 + 0) * a = 0 * a                        Multiplication Property (=)
0 * a + 0 * a = 0 * a                      Distributive Property
0 * a + 0 * a - (0 * a) = 0 * a - (0 * a)  Addition Property
0 * a + 0 = 0                              Inverse Property (+)
0 * a = 0                                  Identity Property (+)
```

proof: First, 0 + 0 = 0 by the identity property of addition. Then, multiplying both sides on the right by a and applying the distributive property, (0 + 0)a = 0a + 0a = 0a. Then add the additive inverse of 0a to both sides on the right, so 0a + 0a + -0a = 0a + -0a. since 0a and -0a are inverses, they add up to 0, so 0a + 0 = 0. Finally, 0a + 0 = 0, again by identity property of addition. Thus 0a = 0. □

### Proof by Contradiction (Indirect Proof)

- Goal: _q_
- Technique:
  - Assume _¬q_.
  - Find a contradiction`(¬p ∧ ¬p)`
- Contradiction:

```
¬q → F   If not q then contradiction.
 T → q   if Tautology then q
 T
 ---
 ∴ q by modus ponens
```

### Proof of a Conjunction

- Goal: `p ∧ q`
- Technique:
  - Prove p.
  - Prove q

> Prove that every multiple of 6 is divisible by both 2 and 3.

1. Prove that every multiple of 6 is divisible by 2.
2. Prove that every multiple of 6 is divisible by 3.

```

```

### Proof of a Disjunction

- Goal : `p ∨ q`
- Technique:
  - Split problem into cases
  - Prove _p_ in some cases and _q_ in other case.

> Theorem: For any integer x, the remainder when x^2 is divided by 4 is either 0 or 1

```
Case 1: x is even     case 2: x is odd
x = 2n                x = 2n + 1
x^2 = (2n)^2          x^2 = (2n + 1)^2
    = 4n^2                = 4n^2 + 4n + 1
```

- Goal: `p ∧ q`
- Technique:
  - Assume _¬p_
  - Prove _q_

> Theorem: For every real number x, if x^2 ≥ x then either x ≤ 0 or x ≥ 1

```
x^2 ≥ x       suppose x is not less or equal to zero, x > 0
x^2 / x ≥ x / x
x ≥ 1
```

### Proof of a Bi-conditional

- Goal: `p ↔ q` (`p → q ∧ q → p`)
- Technique:
  - Prove `p → q`
  - `q → p ⇔ ¬p → ¬q`

> Exercise: Theorem: for any integer x, x is odd if and only if x^2 is odd. Write a proof.

---

## Lecture 5: Set Theory

- A set is a collection of objects. Each object in the set is called a member or an element.
- Roster notation: `{ 1, 2, 3, 4, 5 }`. Roster notation explicitly list every element in the set.
- Sets are represented with capital letters `A = { 1, 2, 3, 4, 5 }`. `a ∈ A` (a is an element of A) for any element a in the set A. `b ∉ A` (b is not an element of A) for any element b not in the set A.
- Elements in a set can not appear more than once. `A = { 1, 2, 2, 3 }` is no a valid set.
- Sets are not ordered.
- Roster notation is not ideal for large sets, thus, set-builder notation is used in such cases: `A = { x|x is an integer between 1 and 5 }` - This reads as the set of all _x_ such that x is an integer between 1 and 5 inclusive, _x_ is a dummy variable.
- Set operators: _⋂ ⋃ \\ ∆_

> Intersection

`A ⋂ B` - The intersection of **A** and **B**

- An intersection checks what elements are common in two sets:

```
A = {1,2,3,4,5}
B = {2,4,6,8,10}

A ⋂ B = {2,4}
```

- _x ∈ A ⋂ B?_
  - _x ∈ A AND x ∈ B_
- Thus: `A ⋂ B ⇔ { x|x ∈ A ∧ x ∈ B }`

> Union

`A ⋃ B` - The union of **A** and **B**

- A union is found by merging two set together, such that each item only appears once in the union, no doubling up on repeats.

```
A = {1,2,3,4,5}
B = {2,4,6,8,10}

A ⋃ B = {1,2,3,4,5,6,8,10}
```

- _x ∈ A ⋃ B?_
  - _x ∈ A OR x ∈ B_
- Thus: `A ⋃ B ⇔ { x|x ∈ A ∨ x ∈ B }`

> Empty Set

```
A = {1,2,3,4,5}
B = {2,4,6,8,10}
C = {1,3,5,7}

B ⋂ C = Ø/{} // Ø null/empty set
```

> Difference

`A \ B` - The difference of **A** and **B**

- The set of everything in the first set that is not in the second set: `A \ B ⇔ { x|x ∈ A ∧ x ∉ B }`

```
A = {1,2,3,4,5}
B = {2,4,6,8,10}

A \ B = {1,3,5}
B \ A = {6,8,10}
```

> Symmetric Difference

`A ∆ B` - The symmetric difference of **A** and **B**

- The symmetric difference is acquired by combining two sets differences: `A ∆ B = A \ B ⋃ B \ A ⇔ { x|x ∈ A ⊻ x ∈ B }`

```
A = {1,2,3,4,5}
B = {2,4,6,8,10}

A ∆ B = {1,3,5,6,8,10}
```

> Subsets

- If set **D** is formed by some elements from set **A** then **D** is a subset of **A**: `D ⊆ A ⇔ x ∈ D → x ∈ A`, if **D** is a subset of **A** then **A** is a superset of **D**: `D ⊇ A //A contains D`.
- If set **C** is not formed by some elements from set **A** then **C** is not a subset of **A**: `C ⊄ A`
- The null set `Ø` is a subset of all sets: `Ø ⊆ A ⇔ a ∈ Ø → a ∈ A`.
- A set is a subset of itself: `A ⊆ A ⇔ x ∈ A → x ∈ A`, but not a proper subset of itself `A ⊄ A`
- `Ø ⊆ A` and `A ⊆ A` are called trivial subsets of **A**.
- **D** is a proper subset of **A** if at least one element of **A** is removed from **D**: `D ⊂ A`

> Equality of Sets

- If **A** is a subset set of **F** and **F** is a subset of **A** then set **A** is equal to set **F**: `A ⊆ F ∧ F ⊆ A ⇔ x ∈ A ↔ x ∈ A`

> Universal Set

- The _universal set_ is the set of everything required in the context of a given problem. ![univeral symbol](https://www.rapidtables.com/math/symbols/set_symbols/U.gif) = `{1,...,n}`

> Complement (Outside a Set)

- `A'` or `A^c` denotes the complement of **A**, which is all the elements that do not belong in set **A**: ![univeral symbol](https://www.rapidtables.com/math/symbols/set_symbols/U.gif) \ A

- `(A ⋂ B)' = A' ⋃ B'`
- `(A ⋃ B)' = A' ⋂ B'`

> Special Sets

1. Natural numbers: ![natural numbers](https://www.rapidtables.com/math/symbols/set_symbols/N.gif) = {1,2,3,4,5,...}.
2. Integer: ![integers](https://www.rapidtables.com/math/symbols/set_symbols/Z.gif) = {...,-2,-1,0,1,2,...}
3. Rational numbers: ![rational symbol](https://www.rapidtables.com/math/symbols/set_symbols/Q.gif). Numbers that can be represented as a ratio.
4. Irrational number ![rational symbol](https://www.rapidtables.com/math/symbols/set_symbols/R.gif) \ ![rational symbol](https://www.rapidtables.com/math/symbols/set_symbols/Q.gif) are numbers that go on forever after the decimal without ever repeating: √2, π, e, e.t.c.
5. Real numbers complete the number line: ![rational symbol](https://www.rapidtables.com/math/symbols/set_symbols/R.gif)
6. Complex number ![rational symbol](https://www.rapidtables.com/math/symbols/set_symbols/C.gif) : `a + bi`, where i is the √-1

> **NOTE: A warning about sets**. A set cannot contain other sets.

> Russell's Paradox

- Let R be the set of all sets that DO NOT contain themselves.

```
R = {A|A ∉ A} //Sets that do not contain themselves
R ∈ R ? // Is R an element of R
R ∉ R → R ∈ R // If R doesn't contain itself then it qualifies as a member or R
R ∈ R → R ∉ R // But if R contain itself then it contradicts its definition as a set that contain sets that don't contain themselves.
```

- A collection that contains sets is called a family of sets.

---

## Lecture 6: Predicate Logic

- A predicate will evaluate as True or False depending on what its argument is.
- p(_x_) = "_x_ is an even number", p(0) = T and p(1) = F. _x_ is a free variable.

### Truth set

- The set of all _x_ for which p(_x_) is True is called the truth set of p.
- Example: for `p(x) = "x is an even number"` the truth set is `{x|p(x)} // The set of all x such that p of x`.

### Quantifiers

- Quantifiers specify how many elements in the set make the predicate to be true.

#### Universal Quantifiers

- `∀ // For all`
- `∀x p(x) // for all x, p(x)`, this mean that p(_x_) is true for all possible x in the universal set.
- Theorem: _For any_ integer _x_, if _x_ is even then _x^2_ is even. `∀x (p(x) → p(x^2))`
- Example:

```
B = {2,4,6,8,10}
∀b ∈ B p(b))
```

#### Existential Quantifier

- `Ǝ // There exists such that`
- `Ǝx p(x) // There exist an x such that p(x)`, p(_x_) is true for at least one possible x in a universal set.
- Example:

```
A = {1,2,3,4,5}
Ǝa (a ∈ A ∧ p(x)) ⇔ Ǝa ∈ A p(x) //There exist a little a in A such that p(a) is true
```

- The truth of a universal quantifier automatically guarantees the truth of an existential quantifier.
- `Ǝ!x p(x) // There exist exactly one x such that p(x)`

### Compound Quantifiers

```
∀xƎy (x < y) // For all x There exist a y such that x is less than y.
Ǝy∀x (x < y) // There exist a y such that all x is less than y
```

### Negation of Quantifiers

- `¬∀x p(x) // it not true that for all x, p(x) is true` equivalent `Ǝx ¬p(x) // there exist x such that p(x) is false`
- `¬Ǝx p(x) // it's not true that there exist x such that p(x) is true` equivalent `∀x ¬p(x) // For all x p(x) is false`

### More on Intersections and Unions

- The intersection of all the sets in family f is defined as `{a|∀A ∈ f (a ∈ A)}`
- The union of all sets in family f is defined as `{a|ƎA ∈ f (a ∈ A)}`

### Index Sets

- An index set is a way to number each element of a set.
