﻿# Spring

---

## Spring in a Nutshell

- A framework for building Java applications
- Initially a simpler and lighweight alternative to J2EE
- Makes things easier by providing a large number of helper classes.
- [Spring 5 release notes](https://www.luv2code.com/spring-5-whats-new)
- [Spring 5 FAQ](https://www.luv2code.com/spring-5-faq)
- [Spring Website official](https://www.spring.io)

---

### Why Spring?

- Simplifies Java Enterprise development
- Lightweight development with Java POJOs (Plain-Old-Java-Objects)
- Dependency injection to promote loose coupling
- Declarative programming with Aspect-Oriented-Programming(AOP)
- Minimize boilerplate Java code

---

#### Core Container

- Factory for managing how beans are created
- Reads configuration files for setting properties and dependencies

**Core Container Components**

1. Beans
2. Core
3. SpEL - (Spring Expression language) - used in config files to refer to other beans.
4. Context -Spring container for holding beans in memory

---

#### Insfrastructure

1. AOP (Aspect Oriented Programmin) - Adds functionality to objects declaratively. Allows for the creation of application wide services i.e logging, security, transactions, etc...
2. Aspects
3. Intrumentation - used to create a Java agents to remotely monitor an application with JMX (Java Management Extension)
4. Messaging

---

#### Data Access Layer

1. Spring JDBC (Java Database Connectivity) Helper classes. Reduces JDBC code by 50%
2. ORM (Object Relational Mapper)
3. Spring has support for a _transaction manager_.
4. JMS (Java Massage Service) used for sending async messages to a message broker. Spring provides helper classes for JMS.

---

#### Web Layer

- All web related classes live in the web module
- Home of the Spring MVC framework.

1. Servlet
2. WebSocket
3. Web
4. Portlet

---

#### Test Layer

- Spring supports Test-Driven-Development (TDD). Mock object and out-of-container testing.

1. Unit
2. Intergration
3. Mock

## Spring Inversion of control

- The design process of externalizing the construction and management of objects. Outsource to an object factory.

- Spring container primary function:

  - Create and manage objects (Inversion of control)

  - Inject object dependencies (Dependency injection)

- Dependency injection using applicationContext.xml example:

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       http://www.springframework.org/schema/context/spring-context.xsd">

    <!-- Define beans-->
    <bean id="coach" class="com.tmoh.BaseBallCoach"/>
</beans>
```

- Fetching bean from the container example:

```
ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

Interface var = context.getBean("bean name", Interface.class);
```

---

## Dependency Injection

- Allows a program design to follow the dependency inversion principle. The client will delegate the XOR code, the injector, the responsibility of providing its dependencies.

### Injection Types

1. Constructor injection
2. Setter Injection

#### Constructor injection

- Class with dependency injection:

```
public class Example {
    private Injected inject;

    public Example(Injected inject) {
        this.inject = inject;
    }
    ...
}
```

- Configure dependency injection in Spring config file:

```
...
<bean id="example" class="com.package.Example">
        <constructor-arg ref="inject" />
</bean>
<bean id="class" class="com.package.Class"/>
...
```

### Setter Injection

- Define a setter method for the dependency injection

```
class SomeClass {
    Service service;
....
    public void setService(Service service) {
        this.service = service;
    }
}
....
```

- Configure the dependency injection in Spring config file

```
....
<bean id="beanId" class="package.name.Service" />

<bean id="anotherBeanId" class="package.name.ClientClass">
    <property name="service(setter arg name)" ref="beanId">
</bean>
....
```

- For every property name in the config file, Spring will attempt to make a call to the setter method for that given property

#### Injecting literal value

- Create literal properties in a class

```
class SomeClass {
   private String email;

    public void setEmail(String email) {
        this.email = email;
    }
}
....
```

- Configure the literal injection in the Spring config file

```
....

<bean id="anotherBeanId" class="package.name.ClientClass">
    <property name="setter arg name" value="literal">
</bean>
....
```

#### Injecting literal value from a Properties File

- Create a Properties File
  File: filename.properties

```
property.propertyname="literal"
```

- Load Properties File in Spring config file

`<context:property-placeholder location="classpath:coach.properties">`

- Reference values from Properties File

```
<bean id="anotherBeanId" class="package.name.ClientClass">
    <property name="setter arg name" value="${property.propertyname}">
</bean>
```

---

### Bean Scopes

- Scope refers to the lifecycle of a bean i.e.
  - how long the bean will live
  - how many instances are created
  - and how the bean is shared
- The default scope for a bean is singleton, meaning Spring will create a single instance of the bean, which will be cached in memory so that all request for the bean will share the same reference
- Singleton is best utilized for a stateless bean.
- Explicitly specifying bean scope:

```
....
<bean id="fortune" class="com.package.SomeClass" scope="singleton"/>
....
```

- The prototype scope creates a new bean instance for each container request.
- The request is scoped to an HTTP web request. Only used for web apps.
- session is scoped to an HTTP web request. Only used for web apps.
- global-session scoped to a global HTTP web session. Only used for web apps.

---

### Bean Lifecycle Methods

#### Bean lifecycle

container started -> bean instantiated -> dependencies injected -> Internal spring processing -> custom init methods -> bean is ready for use -> container shutdown -> custom destroy methods

#### Bean Lifecycle Methods / Hooks

- Custom code can be added during bean initialization
  - Calling custom business logic
  - Setting up handles to resources (db, sockets, file etc)

> Init: method configuration

```
<beans ...>
    <bean id="SomeClass" class="com.package.SomeClass"
     init-method="init method name">
</beans>
```

- Custom code can also be added during bean destruction

  - Calling custom business logic methods
  - Clean up handles to resources (db, sockets, file etc)

  > Destroy: method configuration

```
<beans ...>
    <bean id="SomeClass" class="com.package.SomeClass"
     destroy-method="destroy method name">
</beans>
```

---

## Java Code Configuration

- Configuring the Spring container with Java code requires no XML.

> Three Ways of Configuring Spring Container

1. Full XML Config - list each bean in the XML file
2. XML component scan - uses annotation to scan classes with the annotation _@component_ annotation.
3. Java configuration class - Uses Java source code to configure the container.

> Creating the Configuration Class

1. Create a Java class and annotate as @Configuration

```
@Configuration
public class JavaConfig {

}
```

2. Add component scanning support: _@ComponentScan_ (optional)

```
@Configuration
@ComponentScan("com.package") // scans a package to retrieve beans
public class JavaConfig {

}
```

3. Read the Spring Java configuration class

```
....
AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
....
```

4. Retrieve bean from the Spring container

> Injecting values from Properties File

1. Create a properties file:

```
//File: filename.properties
foo.email=person@email.com
foo.team=zencode
```

2. Load Properties file in Spring config:

```
@Configuration
@PropertySource("classpath:filename.properties")
public class ConfigClass {
    ....
}
```

3. Reference Values from Properties File

```
public class Class {
    @Value("${foo.email}")
    private String email;
    @Value("${foo.team})
    private String team;
    ....
}
```

---

---

## Spring MVC

> What is Spring MVC

- A Framework for building web applications in Java.
- Based on Model-View-Controller design pattern.
- Leverages feature of the core Spring Framework(IoC, DI).

![Model-View-Controller diagram](https://docs.spring.io/spring/docs/3.0.0.M4/spring-framework-reference/html/images/mvc.png)

### Spring MVC Benefits

- Allows for the utilization of reusable UI components to build Java web apps.
- Helps manage application state for web requests
- Process form data: validation, conversion etc
- Flexible configuration for the view layer

### Components of a Spring MVC Application

- A set of web pages to layout UI components
- A collection of Spring beans (controller, services, etc)
- Spring configuration (XML, Annotation or Java)

### Spring MVC Execution Flow

1. First request will be received by DispatcherServlet.
2. DispatcherServlet will take the help of HandlerMapping and get to know the Controller class name associated with the given request.
3. So request transfer to the Controller, and then controller will process the request by executing appropriate methods and returns ModelAndView object (contains Model data and View name) back to the DispatcherServlet.
4. Now DispatcherServlet send the model object to the ViewResolver to get the actual view page.
5. Finally DispatcherServlet will pass the Model object to the View page to display the result.

### Spring MVC Front Controller

- Front controller known as DispatcherServlet
  - Part of the Spring Framework, already developed by the spring team
- To be created
  - Model objects
  - View templates
  - Controller classes

### Controller

- Created by developer
- Contains business logic
  - Handles requests
  - Store / retrieve data (db, web service...)
  - Place data in model
- Send response to appropriate view template.

### Model

- Model: contains web app's data
- Stores and retrieves data via backend systems

### View Template

- Spring MVC is flexible
  - Supports many view templates
- Most common is JSP + JSTL

---

## Spring MVC Configuration Process

### Part 1

> Add configuration to the file: **WEB-INF/web.xml**.

1. Configure Spring MVC Dispatcher Servlet

```
// File: web.xml
<web-app>
  <servlet>
    <servlet-name>dispatcher</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>/WEB-INF/spring-mvc-demo-servlet.xml</param-value>
    </init-param>

    <load-on-startup>1</load-on-startup>
  </servlet>
</web-app>
```

2. Set up a URL mappings to Spring MVC Dispatcher Servlet.

```
// File: web.xml
<web-app>
  <servlet>
    <servlet-name>dispatcher</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    ....
  </servlet>

  <servlet-mapping>
     <servlet-name>dispatcher</servlet-name>
     <url-pattern>/<url-pattern>
  </servlet-mapping>
</web-app>
```

- _servlet-name_ in _servlet-mapping_ should match the _servlet-name_ in the _servlet_

### Part 2

> Add configurations to file: WEB-INF/spring-mvc-demo-servlet.xml

3. Add support for Spring component scanning

```
// spring-mvc-demo-servlet.xml
<beans>
  <!-- Step 3: add support for component scanning -->
  <context:component-scan base-package="com.package.springdemo"/>
</beans>
```

4. Add support for conversion, formatting and validation

```
// spring-mvc-demo-servlet.xml
<beans>
  <!-- Step 3: add support for component scanning -->
  <context:component-scan base-package="com.package.springdemo"/>

  <!-- Step 4: Add support for conversion, formatting and validation support  -->
  <mvc:annotation-driven/>
</beans>
```

5. Configure Spring MVC view resolver

```
// spring-mvc-demo-servlet.xml
<beans>
  <!-- Step 3: add support for component scanning -->
  <context:component-scan base-package="com.package.springdemo"/>

  <!-- Step 4: Add support for conversion, formatting and validation support  -->
  <mvc:annotation-driven/>

  <!-- Step 5: Define Spring MVC view resolver -->
  <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
    <property name="prefix" value="/WEB-INF/view/" />
    <property name="suffix" value=".jsp" />
  </bean>
</beans>
```

### Adding CSS, JavaScript and Images

- Have the following directory structure:

![directory structure](directory-layout.png)

- The resources folder will contain all the assets i.e Javascript files, CSS file and images, the folder name can be any.

1. Add the following entry in the Spring MVC configuration file:

```
<mvc:resources mapping="/resources/**" location="/resources/"></mvc:resources>
```

1. In the view file, access the static files using this syntax:

```
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
```

- This JSP expression `${pageContext.request.contextPath}` is needed to access the root directory of the web application.

### Spring Model

- The Model is a container for the Spring application data

- In the controller
  - The model can hold any data i.e string, objects, info from database, etc...
- The view can access data from the model.
- To read form data in the controller class pass the HttpServletRequest object to the controller.

### Reading Form data

- To read form data use the `HttpServletRequest` or the `@RequestParam()` annotation in the controller.
- When using the annotation Spring will read a param from the request and bind it to a variable:

```
//file.jsp
<input type="text" name="paramName">
....
//controller
@RequestMapping("/processForm")
public String process(@RequestParam("paramName") String param, Model model) {
  // use variable param
}
```

### Adding Request Mappings to Controller

- Request mapping can be defined at controller level, this will serve as a parent mapping for the controller
- All request mapping on methods in the controller will be relative to the controller mapping:

```
@RequestMapping("/home")
public class HomeController {
  @RequestMapping("/form") // url: /home/form
  public String showForm() {

  }
}
```

- This helps in grouping request mappings together, and also useful for resolving conflicting mappings.

### Spring MVC Form Tags

- Spring MVC Form Tags are the building block for a web page
- Form Tags are configurable and reusable for a web page.
- The Form Tags can make use of data binding. Automatically setting and retrieving data from a Java object or bean
- These Form tags will generate HTML behind the scenes:

| Form Tag         | Description           |
| ---------------- | --------------------- |
| form:form        | main form container   |
| form:input       | text field            |
| form:textarea    | multi-line text field |
| form:checkbox    | check box             |
| form:radiobutton | radio buttons         |
| form:select      | drop down list        |
| more ...         |                       |

> Web Page Structure

- JSP page with special Spring MVC Form tags

```
<html>
... regular html ...
... Spring MVC form tags ...
... more html ...
</html>
```

- To reference the Form tags, specify the Spring namespace at the beginning of the JSP file `<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>`

#### Spring MVC Form Tag - Text Field

- Inside the controller, before showing the form add a _model attribute_, this is a bean that will hold form data for _data binding_

```
// controller code snippet
@RequestMapping("/path")
public String showForm(Model model) {
  model.addAttribute("attribute name", new Class() /** value */);

  return "view-form";
}
....

// Spring MVC form jsp file
....
<form:form action="processForm" modelAttribute="attribute name in controller">
  form label1: <form:input path="Class var1" />
  form label2: <form:input path="Class var2" />
  <input type="submit" value="submit" />
</form:from>
....
```

- The _path_ inside the _form:input_ tag, binds the form field to a property inside the bean (attribute).
- When the form is loaded, Spring MVC will call the getter methods inside the bean: `class.getVar1`. And when the form is submitted Spring MVC will call the bean setter methods: `class.setVar1`.

> Handling Form Submission in the Controller

```
@RequestMapping("/path")
public String processForm(@ModelAttribute("attribute name") Class class) {
  return "view";
}
....

// Spring MVC submission view jsp file
....
${attribute name.var1}
${attribute name.var2}
....
```

- The _attribute_ will be populate with form data behind the scenes, no need to do manual request parameter getting.

#### Spring MVC Form Tag - Drop Down List

- In Spring MVC the drop-down list is represented by the tag `form:select`

```
// code snippet
<form:select path="country">
  <form:option value="value1" label="label1"/>
  <form:option value="value2" label="label2"/>
  <form:option value="value3" label="label3"/>
</form:select>
```

- _path_ defines the property for data binding.
- _label_ is what the user will see on the screen and value is the submitted data.
- `form:options items="${class.map property}"`, get the list options from a class.

### Adding Form Options from a Properties File

- Create a properties file to hold the options in **WEB-INF/**.

> NOTE: the location of the properties file is very important. It must be stored in **WEB-INF/** directory.

- Update the header section to include `xmlns:util="http://www.springframework.org/schema/util"` and add the following lines to the `xsi:schemaLocation`:
  - `http://www.springframework.org/schema/util` and
  - `http://www.springframework.org/schema/util/spring-util.xsd`
- Load the properties file in the Spring config file i.e. spring-mvc-servlet.xml: `<util:properties id="options" location="classpath:../filename.properties" />`
- Inject the properties values into a bean or controller.

```
@Value("#{options}")
private Map<String, String> options;
```

### Looping Through a List in View Template

- Include this line in the .jsp file: `<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>`
- To loop through list:

```
<c:forEach var="tmp" items="${attributeName.propertyName}">
  ${tmp}
</c:forEach>
```

---

## Spring MVC Form Validation

- Validation is used to check wether a user entered valid data into a form. Can be used to check user input form for:
  - Required fields
  - Valid number in a range
  - Valid format (postal code)
  - custom validation rules

### Java's Standard Bean Validation API

- Java has a standard Bean validation API, which defines a metadata model and API for entity validations.
- Spring version 4 and higher support Bean Validation API, this is the preferred method for validation when building Spring applications.
- To download the spring validator navigate to [hibernate validator](http://hibernate.org/validator/), and click on the green button to download the validator jar file.
- @Valid annotation performs validation of a class with validation rules:

```
// Validation class\
import javax.validation.constraints.*

public class Validation {
  @NotNull(message="is required")
  @size(min=1, message="is required")
  private String lastName;
}

// validation-form.jsp
....
<form:errors path="attribute" cssClass="error" />
....

//validation controller
....
public String processForm(@Valid @ModelAttribute("customer") Validation validate, BindingResult bindingResult) {
  if (bindingResult.hasErrors()) {
    return "validation-from";
  } else {
    return "validation-confirmation";
  }
}
....
```

- **BindingResult** hold the results of the validation.
- To trim whitespace from the input field use the `@InitBinder`, this annotation works as a preprocessor that pre-processes each web request to the controller.
- Registering a custom editor that trims whitespace from request input in controller:

```
// ExampleController
....
@InitBinder
public void initBinder(WebDataBinder dataBinder) {
  StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

  dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
}
....
```

### Validation with Regular Expressions

- use the `@Pattern(regexp="[regex], message="message"` annotation

### Validation Custom Error Message

- Add a folder named resource in the `src` directory, and create a _messages.properties_ file inside that folder. The location is very important.
- _messages.properties_ file contents: `errorDef.attribute.attributeField=message`
- Add the following lines to the _spring-mvc-demo-servlet.xml_ file:

```
<!-- Load custom message resource -->
<bean id="messageSource" // bean id must be this always
      class="org.springframework.context.support.ResourceBundleMessageSource">
      <property name="basenames" value="resources/messages">
</bean>
```

---

## Hibernate

### What is Hibernate?

- Hibernate is a framework for persisting / saving Java object in a database:

![hibernate and java](https://cdn.visual-paradigm.com/features/v14/4/hibernate-orm-tools/java_object_persistence_with_hibernate.png)

### Benefits of Hibernate

- Hibernate handles all of the low-level SQL
- Minimizes the amount of JDBC code needed in development
- Hibernate provides the Object-to-Relational Mapping (ORM)
- The developer defines mappings between Java class and database table via a configuration file using xml or annotations.

```
// Create Java object
Student theStudent = new Student("Dohn", "Joe", "john@zencode.co.za");

// save to database
int theId = (Integer) session.save(theStudent);

// object retrieval from database
Student student = session.get(Student.class, theId);

// Retrieve all Java Object
Query query = session.createQuery("from Student");

List<Student> students = query.list();
```

### Hibernate and JDBC

- Hibernate uses JDBC for all database communications. Hibernate is just another layer of abstraction on top of JDBC.

### Hibernate Configuration

1. Add Hibernate configuration file:

```
<!-- hibernate.cfg.xml -->
<!DOCTYPE hibernate-configuration PUBLIC
        "-//Hibernate/Hibernate Configuration DTD 3.0//EN"
        "http://www.hibernate.org/xsd/hibernate-configuration-3.0.dtd">
<hibernate-configuration>
    <session-factory>
        <!-- JDBC Database connection settings -->
        <property name="connection.driver_class">org.postgresql.Driver</property>
        <property name="connection.url">jdbc:postgresql://localhost:5432/hb_student_tracker?useSSL=false</property>
        <property name="connection.username">tmoh</property>
        <property name="connection.password">World#4938</property>

        <!-- JDBC connection pool settings ... using built-in test pool -->
        <property name="connection.pool_size">1</property>

        <!-- Select SQL dialect -->
        <property name="dialect">org.hibernate.dialect.PostgresPlusDialect</property>

        <!--Echo the SQL to stdout -->
        <property name="show_sql">true</property>

        <!-- Set the current session context -->
        <property name="current_session_context_class">thread</property>
    </session-factory>
</hibernate-configuration>

```

2. Annotate Java class
   2.1 Map class to database table and field to database columns using annotations

```
@Entity
@Table(name="student")
public class Student {
    @Id
    @Column(name="id")
    private int id;

    @Column(name="first_name")
    private String firstName;
```

3. Develop Java code to perform database operation:

> Terminology

- Entity Class: Java's class that is mapped to a database table.

### SessionFactory

- The **SessionFactory** Reads the hibernate config file, creates a Session object. This object is only created once.
- **Session** is a wrapper around a JDBC connection, it is used to save and retrieve object from the database. It is a short-live object that is retrieved from the **SessionFactory**

```
// Instantiating the SessionFactory
SessionFactory factory = new Configuration()
          .configure()
          .addAnnotatedClass(Student.class)
          .buildSessionFactory();

// Creating and using Session Object
try (Session session = factory.getCurrentSession();) {
  // start transaction
  session.beginTransaction();

  // save object
  session.save(new Class());

  // commit the transaction
  session.getTransaction().commit();
} catch(Exception e) {
  e.printStackTrace();
}
```

### Hibernate Identity - Primary Key

- _@Id_ tells hibernate that the class member is mapped to an primary key id column in the database, this is the short hand which is equivalent to:

```
// A class that maps to a database (orm)
@Entity
@Table(name="entity")
public class Entity {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name="id") //names column to map to
  private int id.
}
```

- Id generation strategies:
  - GenerationType.AUTO - Pick an appropriate strategy for the particular database
  - GenerationType.IDENTITY - Assign primary keys using database identity column
  - GenerationType.SEQUENCE - Assign primary keys using a database sequence
  - GenerationType.Table - Assign primary keys using an underlying database table to ensure uniqueness.
- The custom Id generation strategies can be created using Java code.
  - Create a subclass of **org.hibernate.id.SequenceGenerator**
  - Override the method: public Serializable generate(...)

### Querying a Database

- Hibernate has a query language similar to SQL called **Hibernate Query Language(HQL)**, HQL uses the same predicates a SQL i.e. `WHERE, OR, LIKE`
- Creating a query that retrieve multiple objects from the database:

```
List<Class> objects = session
                      .createQuery("from dbName")
                      .getResultList();
```

- Conditional object retrieval:

```
List<Class> objects = session
                      .createQuery("from dbName where column_name='column_value'")
                      .getResultList();
```

### Updating a Database Record

- Update a single record:

```
// get the session
try (Session session = factory.getCurrentSession()) {
  // begin transaction
  session.beginTransaction();

  // Fetch Object from database
  Entity entity = session.get(Entity.class, id);

  entity.setColumn(column_value);

  // commit transaction
  session.getTransaction().commit();
}
....
```

- Updating multiple records:

```
session
    .createQuery("update Entity set column='column_value');
    .executeUpdate();
```

### Deleting a Database Record

- Delete a single record:

```
// begin transaction
  session.beginTransaction();

  // Fetch Object from database
  Entity entity = session.get(Entity.class, id);

  session.delete(entity);

  // commit transaction
  session.getTransaction().commit();
```

- Deletion without retrieving record:

```
session
    .createQuery("delete from Entity where id=id_value);
    .executeUpdate();
```

---

## Hibernate Advanced Mappings

### Advanced Mapping

- A way of modeling multiple table relations in hibernate.
- Advance mapping types:
  - One-to-One
  - One-to-Many, Many-to-One
  - Many-to-Many

#### One-to-One Mapping

- Mapping between two tables.

#### One-to-Many Mapping

- Mapping between one table and the many tables it relates to, the inverse is Many-to-One.

#### Many-to-Many Mapping

- Mapping between many table and the many tables that they relate to.

### Important Database Concepts

- **Primary key**: identifies a unique row in a table.
- **Foreign key**: Links tables together. A field in one table that refers to primary key in another table.
- **Cascade**: Can cascade operations. Apply the same related operation to related entities.

### Fetch Types: Eager vs Lazy Loading

- Determines how many records are retrieved in one instance:
  - **Eager** will retrieve everything.
  - **Lazy** will retrieve on request.
- 

### Uni-Directional Relationship

- A one way relationship between two related tables.

### Bi-Directional Relationship

- A two way relationship between two related tables.

---

One-to-One Mapping

- One-to-One mapping in Java class:

```
@Entity
@Table(name = "table_name")
public class Example {
  ....
  @OneToOne
  @JoinColumn(name="mapped_table_id")
  private MappedEntityName mappedEntityName;

  ....
}
```

### Entity Life Cycle

| Operation | Description                                                                      |
| --------- | -------------------------------------------------------------------------------- |
| Detach    | If entity is detached, it is not associated with a Hibernate session             |
| Merge     | If instance is detached from session, then merge will reattach to session        |
| Persist   | Transitions new instances to managed state. Next flush / commit will save in db  |
| Remove    | Transitions managed entity to be removed. Next flush/ commit will delete from db |
| Refresh   | Reload / sync object with data from db. Prevent stale data                       |

### @OneToOne - Cascade Types

| CascadeType | Description                                                           |
| ----------- | --------------------------------------------------------------------- |
| Persist     | If entity is persisted / saved, related entity will also be persisted |
| Remove      | If entity is removed / deleted, related entity will also be deleted   |
| Refresh     | If entity is refreshed, related entity will also be refreshed         |
| Detach      | If entity is detached, then related entity will also be detached      |
| Merge       | If entity is merged, then related entity will also be merged          |
| All         | All of the above cascade types                                        |

- Configuring cascade types in Java class:

```
@Entity
@Table(name = "table_name")
public class Example {
  ....
  @OneToOne(cascade=CascadeType.ALL)
  @JoinColumn(name="mapped_table_id")
  private MappedEntity mappedEntity;

  ....
}
```

- Configuring multiple cascade types in Java class:

```
@Entity
@Table(name = "table_name")
public class Example {
  ....
  @OneToOne(cascade={
    CascadeType.DETACH,
    CascadeType.MERGE,
    CascadeType.PERSIST,
    CascadeType.REFRESH,
    CascadeType.REMOVE
  })
  @JoinColumn(name="mapped_table_id")
  private MappedEntity mappedEntity;

  ....
}
```

- By default, no operations are cascaded. Must Explicitly configure cascade operations.
- Creating a bidirectional mapping:

```
@Entity
@Table(name = "table_name2")
public class MappedEntity {
  ....
  @OneToOne(mappedBy="instructorDetail") // Refers to the mappedEntity field in Example class
  private Example example
  ....
}
```

---

## Log4j Logger Configuration

- The log4j logger is used to debug and get Hibernate info during program execution. Configuration in XML:

```
//log4j2.xml
<?xml version="1.0" encoding="UTF-8" ?>
<Configuration xmlns="http://logging.apache.org/log4j/2.0/config">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss} [%t] %-5level %logger{36} - %msg%n"/>
        </Console> <!-- logs data to console -->
        <File name="File" fileName="hibernate.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss} [%t] %-5level %logger{36} - %msg%n"/>
        </File> <!-- logs data to file -->
    </Appenders>
    <Loggers>
        <Root level="info">
            <AppenderRef ref="Console"/>
        </Root>

        <Logger name="org.hibernate.SQL" level="debug">
            <AppenderRef ref="Console"/>
            <AppenderRef ref="File"/>
        </Logger>
    </Loggers>
</Configuration>
```

---


