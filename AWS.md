# AWS Lambdas

## Identity and Access Management (IAM)

- IAM key Concepts
  - Users - access using login credentials or access keys i.e. email and password
  - Groups - collection of users that share access policies
  - Roles - similar to users but without access keys or login credentials, can be assumed by users or AWS services.
