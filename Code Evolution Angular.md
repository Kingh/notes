﻿# Angular

> What and Why

- Javascript Framework for building client side applications.
- Great for SPAs (single page applications), allows for the asynchronous refreshing of application parts without reloading the whole application
- Angular promotes modular approach for a clear structure
- Components allows for reusable code.
- Development is quicker and easier
- Unit testable.

---

## Architecture

> Modules

- An Angular application is a collection of individual modules. Every module represent a feature area within the application.
- Every Angular applications contains a root module named AppModule.
- A module is made up of components and services
- A modules interact and ultimately render the view in the browser

> Components

- A component controls a portion of the view on the browser.
- All components a nested inside the root component(AppComponent).
- Each component has an HTML template for the view, a class for controlling the logic of the component and metadata attached to it.
- The class is simply Typescript code.
- Metadata is what tells Angular wether the class is a component or a regular class, it also defines the selector for the component.
- The metadata is defined using a decorator. To define a component the _@Component_ decorator is used.
- Within the _@Component_ decorator, the HTML template if define as well as the selector which is a custom HTML tag that can be used to represent the component.

> Services

- Services provide the business logic of the application.

> Angular Project Structure

- The _main.ts_ file is the entry point to the Angular application.
- The _app.modules.ts_ is the root module for the application.
- The _app.component.ts_ is the root components of the application.
- When running the command `ng serve` the execution goes to the _main.ts_ file, in the _main.ts_ file Angular bootstraps the AppModule. In the AppModule Angular bootstraps the AppComponent.
- The component contains the HTML view template and and a class to control the component view logic

> Selector Definitions

- There are three way to specify the selector
  1. The first is to specify a selector and use it a custom html tag i.e `<app-test></app-test>`.

```
@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrl: ['./test.component.css']
})
export class TestComponent {}
```

1. The second way is to use it as a CSS class, for that begin the selector with a dot (.) character, then add it to an HTML tag as a class ie `<div class="app-test"></div>`.

```
@Component({
    selector: '.app-test',
    templateUrl: './test.component.html',
    styleUrl: ['./test.component.css']
})
export class TestComponent {}
```

3. Finally the third way is to enclose the selector within square brackets, then add the selector as an attribute to an HTML tag ie `<div app-test></div>`

```
@Component({
    selector: '[app-test]',
    templateUrl: './test.component.html',
    styleUrl: ['./test.component.css']
})
export class TestComponent {}
```

> HTML Template definitions

- Specifying the HTML template:

  - The template metadata can be specified by using _templateUrl_, which point to the file containing the HTML.

    ```
    @Component({
        ....
        templateUrl: './test.component.html',
        ....
    })
    ```

  - Or the HTML can be specified inline inside the component typescript file using _template_.

    ```
    @Component({
        ....
        template: '<h1>Welcome</h1>',
        ....
    })
    ```

> CSS Style definitions

- Specifying CSS styles:

  - CSS styles can be defined using the _styleUrls_, which is an array of files containing CSS files.

    ```
    @Component({
        ....
        styleUrls: ['./test.component.css'],
        ....
    })
    ```

  - The other way to specify CSS styles is to use _style_. which is an array of multiple inline styles

        ```
        @Component({
            ....
            styleUrls: [
                `
                div {
                    color: red;
                }
                `
            ],
            ....
        })
        ```

---

## Interpolation

- Interpolation allows the specifying of properties within templates:

```
@Component({
    ....
    template: '<h1>Welcome {{ name }}</h1>',
    ....
})
export class TestComponent {
    public name = 'Test';
    ....
}
```

- Interpolation also allows for the evaluation of expression withing templates, string concatenation and method invocation within the curly braces:

```
@Component({
    ....
    template: '
        <h2>
            {{ 2 + 2 }}
        </h2>
        <h2>
           {{ 'Name' + name }}
        </h2>
        <h2>
            {{ name.length }}
        </h2>
        <h2>
            {{ name.toUpperCase() }}
        </h2>
        ',
    ....
})
export class TestComponent {
    public name = 'Test';
    ....
}
```

- Variable assignment is not allowed in interpolation, this causes a parse error.
- Global variables are also not available with interpolation. ie `<h2>{{ window.location.href }}</h2>`. This gives an error as window is undefined within the template. Access to the global variables can be done within the component class then bound to the template.

---

## Property Binding

> Attribute vs Property

- Attributes and Properties are not the same.
- Attributes are defined by HTML.
- Properties are defined by the DOM (Document Object Model).
- Attributes initialize DOM properties and then they are done. Attribute values cannot change once initialized.
- Property values can change.

> Why Property Binding

- Interpolation only works with string values, that is why property binding is used when working with attributes that are not strings.
- Property binding:
  - `<input [disabled]="false" type="text" value="value">`.
  - `<input bind-disabled="false" type="text" value="value">`.

---

## Class Binding

- To use class-binding, declare a property in the component class and assign to it the class name, then use the square syntax to bind the class to the HTML element.

```
@Component(
    <h2 [class]="successClass"></h2>
)
export class Component implements OnInit {
    public successClass = "text-success";
    ....
}
```

- A regular class attribute becomes a dummy in the presence of class-binding, use one or the other but not both.
- Another syntax of class binding provides the ability to apply a class based on an expression that evaluates to boolean values.

```
@Component(
    <h2 [class.text-danger]="hasError"></h2>
)
export class Component implements OnInit {
    public hasError = true;
    ....
}
```

- To apply multiple conditional classes use the _NgClass_ directive. A directive is a custom HTML attribute provided by angular:

```
@Component(
    <h2 [ngClass]="messageClass"></h2>
)
export class Component implements OnInit {
    public isSpecial = true;
    public hasError = true;
    public successClass = "text-success";
    public messageClasses = {
        "text-success": !this.hasError,
        "text-danger": hasError,
        "text-special": this.isSpecial
    }
    ....
}
```

- _ngClass_ allows for the dynamic removal and addition of CSS classes, this directive takes an object as an argument. The format of the object: `{'css class': true/false}`, the object should contain the class as a key and an expression that evaluates to boolean **true/false**.
- Class binding allows for the dynamic addition and removal of classes to HTML elements based on application state or user input.

---

## Style Binding

- Used to apply inline styles to HTML elements.

```
@Component(
    <h2 [style.color]="'orange'">Style Binding</h2>
)
```

- It's also possible to style bind using conditional (ternary operator):

```
@Component(
    <h2 [style.color]="hasError ? 'red' : 'green'">Style Binding</h2>
)
export class Component implements OnInit {
    public hasError = true;
    ...
}
```

- Component class properties can be assigned to style binding

```
@Component(
    <h2 [style.color]="highlightColor">Style Binding</h2>
)
export class Component implements OnInit {
    public highlightColor = "orange";
    ...
}
```

- To apply multiple styles use the ngStyle directive. The _ngStyle_ directive takes a javascript object as argument.

```
@Component(
    <h2 [ngStyle]="titleStyles"></h2>
)
export class Component implements OnInit {
    public isSpecial = true;
    public hasError = true;
    public successClass = "text-success";
    public titleStyles = {
        color: "blue",
        fontStyle: "italic"
    };
    ....
}
```

## Event Binding

- Captures user event, from template to class:

```
@Component(
    <button (click)="onClick($event)"></button>
    {{ greeting }}
)
export class Component implements OnInit {
    public greeting;
    constructor () {}

    ngOnInit() {}

    onClick(event) {
        greeting = 'Sets greeting on click';
    }
}
```

- \$event capture the event information, passed to the event handler.
- Event binding can also be used to assign properties:

```
@Component(
    <button (click)="greeting='Some message'"></button>
    {{ greeting }}
)
export class Component implements OnInit {
    public greeting;

    constructor () {}

    ngOnInit() {}
}
```

## Template Reference Variables

- Used to easily access DOM elements and their properties.

```
@Component(
    <iput type="text" #input>
    <button (click)="logMessage=(input.value)">Log</button>
)
export class Component implements OnInit {
    public greeting;

    constructor () {}

    ngOnInit() {}

    logMessage(value) {
        console.log(value);
    }
}
```

## Two Way Binding

- Allows for the simultaneous update and display of a property. For two way binding angular provide the _ngModel_ directive.

```
@Component(
    <iput [(ngModel)]="name" type="text" #input>
    {{ name }}
)
export class Component implements OnInit {
    public name: string;

    constructor () {}

    ngOnInit() {}
}
```

- To enable two way data binding import the forms module in the app.module.ts file `import { FormsModule } from '@angular/forms'`, then add it to the imports array.

---

## Structural Directives

- Used to add or remove HTML elements from the DOM:
  - **ngIf**
  - **ngSwitch**
  - **ngFor**
- **ngIf** and **ngSwitch** used to conditionally render HTML elements, **ngFor** is used to render a list of HTML elements.

### ngIf

- Using **ngIf**:

```
@Component({
    selector: 'app-test',
    template: `
        <h2 *ngIf=displayName", else elseBlock>
            ZenCode
        </h2>
        <ng-template #elseBlock>
            <h2>
                name is hidden
            </h2>
        </ng-template>
    `,
    ....
})
export class TestComponent implements OnInit {
    private displayName: boolean = false;
    ....
}
```

- The above code uses **ngIf** to check the truthy value of displayName, if it's true it renders the element, otherwise it check if there is an else statement and renders the element of the else statement if it exists.
- The `<ng-template>` tag is a container for other elements, that the **ngIf** statement can use to properly add or remove HTML elements from the DOM.

> Alternate syntax

```
@Component({
    selector: 'app-test',
    template: `
        <div *ngIf="displayName">
            <h2>ZenCode</h2>
        </div> // without an else

        <div *ngIf="displayName; then thenBlock; else elseBlock">
        <ng-template #thenBlock>
            <h2>ZenCode</h2>
        </ng-template>
        <ng-template #elseBlock>
            <h2>Hidden</h2>
        </ng-template>
    `,
    ....
})
export class TestComponent implements OnInit {
    private displayName: boolean = false;
    ....
}
```

### ngSwitch

- **ngSwitch** is like the _switch_ statement found in most programming languages. The only difference is that **ngSwitch** renders HTML instead of executing logic.
- **ngSwitch** is used when comparing multiple values

```
@Component({
    selector: 'app-test',
    template: `
        <div [ngSwitch]="color">
            <div *ngSwitchCase="'red'">red color picked</div>
            <div *ngSwitchCase="'blue'">blue color picked</div>
            <div *ngSwitchCase="'green'">green color picked</div>
            <div *ngSwitchDefault>pick color again</div>
        </div>
    `,
    ....
})
export class TestComponent implements OnInit {
    private color: string = "red";
    ....
}
```

### ngFor

- The **ngFor** is like the _for_ loop found in most programming languages except that it is used to render a list of HTML elements instead of executing logic.

```
@Component({
    selector: 'app-test',
    template: `
        <div *ngFor="let color of colors; index as i //gets the index of the element; first as f // indicates if element is the  first in array; last as l // indicates if element is the last; odd as o //get odd elements; even as e //gets even element">
            <ul>
                <li>{{i}} {{ color }}</li>
            </ul>
        </div>
    `,
    ....
})
export class TestComponent implements OnInit {
    private colors: string[] = ["red", "blue", "green", "yellow"];
    ....
}
```

---

## Component Interaction

- In the above examples the _AppComponent_ is the parent component and the _TestComponent_ is the child component since it's nested inside the _AppComponent_
- To allow data sharing between components we use the @Input and @Output decorator. Using the @Input decorator the child component can accept input from the parent and using the @Output decorator it can send events to the parent to indicate something.
- Sending data from parent to the child component:

```
// Parent component class AppComponent
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public name = "dishwash";
}

// app component template app.component.html
<app-test [parentData]="name"></app-test>

// Child component class TestComponent
@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css']
})
export class TestComponent {
    @Input(parentData) public name: string;
}

// app component template test.component.html
<h2>{{ name }}</h2>
```

- Data is sent to the parent component via _EvenEmitter_ and the @Output decorator.
- Sending data from child to the parent component:

```
// Parent component class AppComponent
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public name = "dishwash";
    pubic message: string;
}

// app component template app.component.html
<app-test (childEvent)="message=$event" [parentData]="name"></app-test>

// Child component class TestComponent
@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css']
})
export class TestComponent {
    @Input(parentData) public name: string;

    @Output() public childEvent = new EventEmitter();

    fireEvent() {
        this.childEvent.emit('Hey ZenCode');
    }
}

// app component template test.component.html
<h2>{{ name }}</h2>
<button (click)="fireEvent()">Send Event</button>
```

> summary

- To send data from the parent to the child declare a property in the parent class and bind it to the child component selector, then use the @Input decorator to access the property in the child class. The same name bound to the selector can be used in the child class or an alias can be used, to use an alias declare the bind name in the bracket of the @Input decorator and declare a new name outside the brackets `@Input(parentData) public name: string;`.
- To bind data from child to parent component use EventEmitter and @Output decorator. Create an instance of the EventEmitter class and emit it from the child component using the @Output decorator. The parent component the listens to the event that executes a template statement or a method, the \$event variable gives access to the argument sent from the child event. The value can be assigned to a property in the parent component.

---

## Pipes

- Pipes allow for the transformation of data before displaying in the view.

### String Pipes

- The _lowercase_ pipe converts a string to lowercase:

```
<h2>{{ name | tolowercase }}</h2>
```

- The _uppercase_ pipe converts a string to uppercase:

```
<h2>{{ name | uppercase }}</h2>
```

- The _titlecase_ pipe converts the first letter of every word in a string to uppercase:

```
<h2>{{ message | titlecase }}</h2> // Welcome To Codevolution
....
public message = "Welcome to codevolution";
```

- The _slice_ pipe returns a substring from a given string, it takes an argument stating where the substring returned will start.

```
<h2>{{ name | slice:3 }}</h2> // output string to dom starting at 3
<h2>{{ name | slice:3:5 }}</h2> // outputs ev to the dom. 3-start index, 5-end index not inclusive
....
public name = Codevolution;
```

- The _json_ pipe returns a JSON representation of an object.

```
<h2>{{ people | json }}</h2> // {"firsName": "John", "lastName": "Doe"}
....
public person = {
    "firstName": "John",
    "lastName": "Doe"
}
```

### Number Pipes

- The _number_ pipe returns a number formatted as specified by the argument. _number_ pipe general form: `value | pipe:'d.d-d'`, the value is any number, pipe is the _number_ pipe, the ds are numbers from 0-9, the first number specifies the number of digits before the decimal, the second number specifies the minimum number of decimal digits and the last number specifies the max number of decimal digits.

```
<h2>{{ 5.678 | number:'1.2-3' }}</h2> // output: 5.678
<h2>{{ 5.678 | number:'3.4-5' }}</h2> // output: 005.6780
<h2>{{ 5.678 | number:'3.1-2' }}</h2> // output: 005.68
```

- The _percent_ pipe returns a percentage representation of a number.

```
<h2>{{ 0.25 | percent }}</h2> // output: 25%
```

- The _currency_ pipe output the number in currency format. The default currency is the dollar, but the pipe takes an argument for currency. A third argument can be parsed to get the currency code instead of the currency symbol.

```
<h2>{{ 0.25 | currency }}</h2> // output: $0.25
<h2>{{ 0.25 | currency: 'GBP' }}</h2> // output: £0.25
<h2>{{ 0.25 | currency: 'GBP':'code' }}</h2> // output: £0.25
```

### Date Pipe

- The date pipe is used for working with different date formats.

```
<h2>{ date | date:'short' }</h2> // dd/m/yy, h:mm PM/AM
<h2>{ date | date:'shortDate' }</h2> // dd/m/yy
<h2>{ date | date:'shortTime' }</h2> // h:mm PM/AM
....
public date = new Date();
```

---

## Services

- A service is a class with a specific purpose. Service uses:
  - Share data across components.
  - Implementation of application logic.
  - External interactions such as connecting to a database.
- The service naming convention is to end the service name with `.service.ts`

### Dependency Injection

- DI is a coding pattern in which a class receives its dependencies from external sources rather than creating them itself.
- Angular's DI as a framework has an injector that acts a a container for all registered dependencies.
- Using a service in angular:
  - Define a service class
  - Register the service with Injector
  - Declare as dependency in another service or component.

### Using a Service

- To generate a service use the Angular CLI to run the command `ng g (generate) s (service) service-name`.
- To be used, the service needs to be registered with the Angular Injector else it will be treated as just another regular class by Angular.
- The service can be injected in multiple places, but it is important to consider where it is injected as dependency injection in Angular is hierarchical. This means that when a service is registered to a specific component, it will only be available to that component and its children.
- To register a service, include it in the _providers_ array inside the _module.ts_ file
- To use the service, declare it in the component that needs to use it.
- The @Injectable decorator tells Angular that a service might have injected dependencies.

---

## HTTP and Observables

### Observables

- An _Observable_ is a sequence of items that arrive asynchronously over time, In the context of HTTP call it's a single item.

> Summary

1. Make HTTP Get request from a service
2. Receive an _observable_ and do casts if necessary
3. Subscribe to the _observable_ in components

> Importing the HttpClientModule into the Application

```
// app.module.ts
....
import { HttpClientModule } from '@angular/common/http';
....
import: [
    BrowserModule,
    HttpClientModule // Also register the Http service with Angular Injector no need for explicit registration in providers
],
....
```

> Using the Http Service In Application's service

```
// example.service.ts
....
export class ExampleService {
    constructor(private http: HttpClient) {

    }

    // utilizing the HttpClient
    getData(): Observable<Data[]> {
        return this.http.get<Data[]>(this._url).pipe(
            catchError((err: HttpErrorResponse) => {
                return throwError(err);
            })
        );
    }
}
....
```

> Using the Service

```
// example.component.ts
....
export class ExampleComponent implements OnInit {
    public data = [];

    constructor(private _employeeService: EmployeeService) {}

    ngOnInit() {
        this._employeeService.getData()
            .subscribe((data) => this.data = data);
    }
}
....
```

---

## Routing in Angular

- In index.html add base tag inside the `<head></head>` tag: `<base href="/">`
- In the app `app-routing.module.ts` add the path definitions to the _routes_ array, each route is an object containing a path and a corresponding component.

```
const routes: Routes = [
    { path: '', redirectTo: '/route1', pathMatch: 'full'}, // default route redirect to route1
    { path: 'route1', component: Route1Component},
    { path: 'route2', component: Route2Component},
    { path: '**', component: PageNotFoundComponent} // all unknown routes
]
```

- After defining the routes pass the _routes_ array to the `RouterModule.forRoot(routes)` function.
- Export the _AppRoutingModule_ and the _routingComponents_.
- Import both in the `app.module.ts` and include them in the imports array.
- In the `app.component.html` create _nav_ buttons with the `routerLink` directive for navigation using the routes:

```
<nav>
  <a routerLink="/route1" routerLinkActive="active">Route1</a>
  <a routerLink="/route2" routerLinkActive="active">Route2</a>
</nav>
```

- The `routerLinkActive` directive changes the style of the button
- Add the `<router-outlet></router-outlet>` tag at the bottom of the app.component.html which help switch to the content of the selected route.

### Wildcard Route and Redirecting Routes

- Wildcard route should always be the last route in the _routes_ array, because the router attempt to match from the top.

### Route Parameters

- Create a route with a placeholder for a parameter:

```
// app-routing.module.ts
const routes: Routes = [
    ....
    { path: "/route3/:param", component: Route3Component},
    { path: "**", component: PageNotFoundComponent}
]
```

- To parse a parameter while navigating use the Router service which has a navigate method: `this.router.navigate(['/route3', parameter])`, this should be in the component to navigate away from.
- To read the parameter use the ActivatedRoute service: `this.route.snapshot.paramMap.get('id')`, this is placed in the component navigated to.

### Optional Route Parameters

- optional parameters: `this.router.navigate(['/route', {paramKey: param}])`
- To retrieve a parameter from a route use:

```
import {ActivatedRoute, ParamMap} from '@angular/router';

constructor(private route: ActivatedRoute){}

ngOnInit() {
    this.route.paramMap.subscribe((param: ParamMap) => {
        param.get('paramKey');
    })
}
```

### Relative Navigation

- Absolute paths are identified by the forward slash before the pathname.
- Defining a relative path: `this.router.navigate([parameter], {relativeTo: this.route})`
