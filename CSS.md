﻿﻿# CSS

---

---

> Basic CSS: Change the color of Text

- The color of an element's text can be changed by changing the _style_ of that element.
- The _style_ property responsible for the color of an element's text is the _color_ style property. <break>
  example of how to set the color of an _h2_ element's text to blue:
  `<h2 style="color: blue;">Header Text </h2>`

---

> Basic CSS: Use the CSS Selectors to Style Elements

- With CSS, there are hundreds of CSS properties that can be used to change the way an element looks on a page.
- The example above styled the individual element with inline CSS (Cascading Style Sheets).
- The other way to specify the style of an element is to define a style block inside the header of an html file
  - example of how to set the color of an _h2_ element's text to blue using a style block:

```
<style>
	h2 {
	   color: red;
	}
</style>
```

---

> Basic CSS: Use a CSS Class to Style an Element

- Classes are reusable styles that can be added to HTML elements.

  - Here's an example:

```
 <style>
	.blue-text {
	   color: red;
	}
</style>
```

- a CSS class is created within the `<style>` tag in the above example.
- The class can be applied to an HTML element like this: `<h2 class="blue-text";">Header Text </h2>`
  **NOTE** that in the CSS style element, class starts with a period. In the HTML elements' class attribute, the class name doesn't include the period.

---

> Basic CSS: Style Multiple Elements with a CSS Class

- Classes allow you to use the same CSS styles on multiple HTML elements.

---

> Basic CSS: Change the Font Size of an Element

- Font size is controlled by the _font-size_ CSS property, like this:

```
h1 {
   font-size: 30px;
}
```

---

> Basic CSS: Set the Font Family of an Element

- The _font-family_ property set the font an HTML element should use
  - For example, to set the font of an _h2_ element to _sans-serif_ use:

```
h2 {
  font-family: sans-serif;
}
```

---

> Basic CSS: Import a Google Font

- In addition to specifying font found on most operating system, custom non-standard web fonts can be specified for use on a website.
- There are many sources for web fonts on the internet.
- Google Fonts is a free library of web fonts that can be used in CSS by referencing the font's URL. Here is how to import the _Lobster_ font:

`<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">`

---

> Basic CSS: Specify How Fonts Should Degrade

- There are several default fonts that are available in all browsers. These generic fonts families include _monospace_, _serif_ and _sans-serif_
- when one font is not available, the browser can degrade to another font, to do that specify a second font as follows:

```
p {
   font-family: Helvetica, sans-serif;
}
```

- Generic font family names are not case sensitive. They also don't need quotes because they are CSS keywords.

---

> Basic CSS: Size Images

- CSS has a property called _width_ that controls an element's width. Just like with fonts, use px (pixels) to specify the image's width. <break>
  for example, to create a CSS class called _larger-image_ that gives HTML elements a width of 500 pixels, use:

```
<style>
     .large-image {
     	width: 500px;
     }
</style>
```

---

> Basic CSS: Add Borders Around Elements

- CSS borders have properties like _style_, _color_ and _width_ <break>
  for example, to create a red, 5 pixel border around an HTML element, use this class:

```
<style>
     .thin-red-border {
     	border-color: red;
        border-width: 5px;
        border-style: solid;
     }
</style>
```

---

> Basic CSS: Add Rounded Corners with border-radius

- To round corners on an element with a border use the CSS property called _border-radius_
- _border-radius_ is specified with pixels

---

> Basic CSS: Make Circular Images with a border-radius

- In addition to pixels, _border-radius_ can be specified using a percentage.

---

> Basic CSS: Give a Background Color to a div Element

- The _background-color_ property sets an element's background color <break>
  for example to set an element's background color to green, add the following to the _style_ element:

```
.green-background {
   background-color: green;
}
```

---

> Basic CSS: Set the id of an Element

- In addition to classes, each HTML element can also have an _id_ attribute.
- There are several benefits to using _id_ attributes:
  - _id_ can be used to style a single element and can also be used to select and modify specific elements with javascript.
  - _id_ attributes should be unique. Browsers don't enforce this, it's just a widely agreed upon practice. <break>
    **NOTE** don't give more than one element the same _id_ attribute.

---

> Basic CSS: Use an id Attribute to Style an Element

- The _id_ attribute can be used to style an element, however unlike classes, it is not reusable and should only be applied to one element.
- An _id_ has a higher specificity (importance) than a class, so if both are applied to the same element and have conflicting styles, the styles of the _id_ will be applied.<break>
  _id_ example:

```
#element-id {
   background-color: green;
}
```

---

> Basic CSS: Adjust the Padding of an Element

- All HTML elements are essentially little rectangles.
- Three important properties control the space that surrounds each HTML element:
  - _padding_
  - _margin_
  - _border_
- An element's _padding_ controls the amount of space between the element's content and its _border_
- When increasing an element's _padding_ it will increase the distance (_padding_) between the content inside the element and the border around it.

---

> Basic CSS: Adjust the Margin of an Element

- An element's _margin_ controls the amount of space between an element's _border_ and the surrounding elements.
- When increasing an element's _margin_, it will increase the distance between its border and surrounding elements.
- If an element's _margin_ is set to a negative value, the element will grow large.

---

> Basic CSS: Add Different Padding to Each Side of an Element

- CSS allows the control of the _padding_ of all four individual sides of an element with the:
  - _padding-top_
  - _padding-right_
  - _padding-bottom_
  - _padding-right_

---

> Basic CSS: Add Different Margins to Each Side of an Element

- CSS allows the control of the _margin_ of all four individual sides of an element with the:
  - _margin-top_
  - _margin-right_
  - _margin-bottom_
  - _margin-left_

---

> Basic CSS: Use Clockwise Notation to Specify the Padding of an Element

- Instead of specifying an element's _padding-top_, _padding-right_, _padding-bottom_ and _padding-left_ properties individually, they can be specified all in one line like this: `padding: 10px 20px 10px 20px;`
- This four values work like a clock: top, right, bottom, left and will produce the exact same result as using the side-specific padding instructions.

---

> Basic CSS: Use Clockwise Notation to Specify the Margin of an Element

- Instead of specifying an element's _margin-top_, _margin-right_, _margin-bottom_ and _margin-left_ properties individually, they can be specified all in one line like this: `margin: 10px 20px 10px 20px;`
- This four values work like a clock: top, right, bottom, left and will produce the exact same result as using the side-specific padding instructions.

---

> Basic CSS: Use Attribute Selectors to Style Elements

- Apart from _id_ and _class_ attribute there are other CSS selectors that can be used to select custom groups of elements to style. <break>
  using the _[attr=value]_ attribute selector:

```
[type='radio'] {
  margin: 20px 0px 20px 0px;
}
```

---

> Basic CSS: Understand absolute versus Relative Units

- The previous examples set an element's margin or padding using pixel (px). Pixels are a type of length unit, which is what tells the browser how to size or space an item.
- In addition to _px_, CSS has a number of different length unit options to use.
- The two main type of length units are absolute and relative.
  <break>
- Absolute units tie to physical units of length. For example _in_ and _mm_ refer to inches and millimeters, respectively.
- Absolute length units approximate the actual measurement on a screen, but there are some differences depending on a screen's resolution.
  <break>
- Relative units, such as _em_ or _rem_, are relative to another length value. For example, _em_ is based on the size of an element's font.
- When used to set the _font-size_ property itself, it's relative to the parent's _font-size_.

---

> Basic CSS: Style the HTML Body Element

- Every HTML page has a _body_ element<break>
  example of how to set the background-color of the whole page:

```
<style>
  body {
    background-color: black;
  }
</style>
```

---

> Basic CSS: Inherit Styles from the Body Element

```
<style>
  body {
    background-color: black;
    font-family: monospace;
    color: green;
  }
</style>
<h1>Hello World!</h1>
```

- The _h1_ element inherits its style from the _body_ element.

---

> Basic CSS: Prioritize One Style Over Another

- Sometimes HTML elements will receive multiple styles that conflict with one another.
  i.e an _h1_ element can't be both green and pink at the same time

```
<style>
  body {
    background-color: black;
    font-family: monospace;
    color: green;
  }
  .pink-text {
    color: pink;
  }
</style>
<h1 class="pink-text">Hello World!</h1>
```

---

> Basic CSS: Override Styles in Subsequent CSS

- In the previous example the "pink-text" class overrode the _body_ element's CSS declaration!
- This proves that classes will override the _body_ element's CSS.
- To override the "pink-text" class declare a class below it like this:

```
<style>
  body {
    background-color: black;
    font-family: monospace;
    color: green;
  }
  .pink-text {
    color: pink;
  }
  .blue-text {
    color: blue;
  }
</style>
<h1 class="pink-text blue-text">Hello World!</h1>
```

---

> Basic CSS: Override Class Declaration by Styling ID Attribute

- The preceding code example proves that browsers read CSS from top to bottom in order of their declaration.
- This means that, in the event of a conflict, the browser will use whichever CSS declaration came last.
- As previously stated the _id_ attribute takes precedence over classes. example:

```
<style>
  body {
    background-color: black;
    font-family: monospace;
    color: green;
  }
  #orange-text {
    color: orange;
  }

  .pink-text {
    color: pink;
  }
  .blue-text {
    color: blue;
  }

</style>
<h1 id="orange-text" class="pink-text blue-text">Hello World!</h1>
```

- The above example proves that _id_ declaration override _class_ declaration despite the order they appear in, in the CSS.

---

> Basic CSS: Override Class Declaration with Inline Styles

- There are other ways to override CSS. Inline style have a higher precedence.

`<h1 style="color: green;">header tag</h1>`

---

> Basic CSS: Override All Other Styles by using Important

- _!important_ has a higher precedence than all style declarations. Example:
  `color: red !important`

---

> Basic CSS: Use Hex Code for Specific Colors

- There are other way to represent color in CSS.
- Hexadecimal code (hex code) is another way to represent color.

```
body {
  color: #000000;
}
```

---

> Basic CSS: Use Hex Code to Mix Colors

- Hex codes use 6 hexadecimal digits to represent colors, two each for red (R), green (G), blue (B) components.
- From these three pure colors (red, green and blue), they can be varied to produce over 16 million colors!
- For example, orange is pure red, mixed with some green and no blue.
- In hex code, this translates to being `#FFA500`

---

> Basic CSS: Use Abbreviated Hex Code

- Hex codes can be abbreviated to use 3 digits, first digit for red, the second digit for green and the third for blue. i.e `#F00` for the color red.
- This reduces the total number of possible colors to 4,000.
- Browsers still interpret `#F00` as `FF0000`

---

> Basic CSS: Use RGB value to Color Elements

- Another way to represent color in CSS is by using _RGB_ values
- The RGB value for black is: `rgb(0, 0, 0)`
- This format specifies the brightness of each color with a number between 0 to 255
- _RGB_ has the same number of possible values as hex code.

---

> Basic CSS: Use RGB to Mix Colors

- Same as hex code, _RGB_ can mix colors by using combinations of different values.

| Color  | RGB                |
| :----- | ------------------ |
| Blue   | rgb(0, 0, 255)     |
| red    | rgb(255, 0, 0)     |
| Orchid | rgb(218, 112, 214) |
| Sienna | rgb(160, 82, 45)   |

---

> Basic CSS: Use CSS Variables to change several elements at once

- CSS variable are used to change CSS style properties at once by changing one value <break>
  example:

```
.class1 {
    --penguin-skin: black;
    --penguin-belly: gray;
    --penguin-beak: yellow;
}

.class2 {
    background: var(--penguin-skin, gray);
}
```

---

> Basic CSS: Create a custom CSS Variable

- To create a CSS variable, give the variable name two hyphens in front of it and assign it a value like this: `--element-one: gray;`
- The above creates a variable named _--penguin-skin_ and assings it the value gray. Now this variable can be used anywhere in CSS to change the value od other elements to gray.

---

> **FOR FUN:** How to create a penguin in CSS

```
 <style>
  :root {
    --penguin-size: 300px;
    --penguin-skin: gray;
    --penguin-belly: white;
    --penguin-beak: orange;
  }

  @media (max-width: 350px) {
    :root {

      /* add code below */

      /* add code above */

    }
  }

  .penguin {
    position: relative;
    margin: auto;
    display: block;
    margin-top: 5%;
    width: var(--penguin-size, 300px);
    height: var(--penguin-size, 300px);
  }

  .right-cheek {
    top: 15%;
    left: 35%;
    background: var(--penguin-belly, white);
    width: 60%;
    height: 70%;
    border-radius: 70% 70% 60% 60%;
  }

  .left-cheek {
    top: 15%;
    left: 5%;
    background: var(--penguin-belly, white);
    width: 60%;
    height: 70%;
    border-radius: 70% 70% 60% 60%;
  }

  .belly {
    top: 60%;
    left: 2.5%;
    background: var(--penguin-belly, white);
    width: 95%;
    height: 100%;
    border-radius: 120% 120% 100% 100%;
  }

  .penguin-top {
    top: 10%;
    left: 25%;
    background: var(--penguin-skin, gray);
    width: 50%;
    height: 45%;
    border-radius: 70% 70% 60% 60%;
  }

  .penguin-bottom {
    top: 40%;
    left: 23.5%;
    background: var(--penguin-skin, gray);
    width: 53%;
    height: 45%;
    border-radius: 70% 70% 100% 100%;
  }

  .right-hand {
    top: 5%;
    left: 25%;
    background: var(--penguin-skin, black);
    width: 30%;
    height: 60%;
    border-radius: 30% 30% 120% 30%;
    transform: rotate(130deg);
    z-index: -1;
    animation-duration: 3s;
    animation-name: wave;
    animation-iteration-count: infinite;
    transform-origin:0% 0%;
    animation-timing-function: linear;
  }

  @keyframes wave {
      10% {
        transform: rotate(110deg);
      }
      20% {
        transform: rotate(130deg);
      }
      30% {
        transform: rotate(110deg);
      }
      40% {
        transform: rotate(130deg);
      }
    }

  .left-hand {
    top: 0%;
    left: 75%;
    background: var(--penguin-skin, gray);
    width: 30%;
    height: 60%;
    border-radius: 30% 30% 30% 120%;
    transform: rotate(-45deg);
    z-index: -1;
  }

  .right-feet {
    top: 85%;
    left: 60%;
    background: var(--penguin-beak, orange);
    width: 15%;
    height: 30%;
    border-radius: 50% 50% 50% 50%;
    transform: rotate(-80deg);
    z-index: -2222;
  }

  .left-feet {
    top: 85%;
    left: 25%;
    background: var(--penguin-beak, orange);
    width: 15%;
    height: 30%;
    border-radius: 50% 50% 50% 50%;
    transform: rotate(80deg);
    z-index: -2222;
  }

  .right-eye {
    top: 45%;
    left: 60%;
    background: black;
    width: 15%;
    height: 17%;
    border-radius: 50%;
  }

  .left-eye {
    top: 45%;
    left: 25%;
    background: black;
    width: 15%;
    height: 17%;
    border-radius: 50%;
  }

  .sparkle {
    top: 25%;
    left:-23%;
    background: white;
    width: 150%;
    height: 100%;
    border-radius: 50%;
  }

  .blush-right {
    top: 65%;
    left: 15%;
    background: pink;
    width: 15%;
    height: 10%;
    border-radius: 50%;
  }

  .blush-left {
    top: 65%;
    left: 70%;
    background: pink;
    width: 15%;
    height: 10%;
    border-radius: 50%;
  }

  .beak-top {
    top: 60%;
    left: 40%;
    background: var(--penguin-beak, orange);
    width: 20%;
    height: 10%;
    border-radius: 50%;
  }

  .beak-bottom {
    top: 65%;
    left: 42%;
    background: var(--penguin-beak, orange);
    width: 16%;
    height: 10%;
    border-radius: 50%;
  }

  body {
    background:#c6faf1;
  }

  .penguin * {
    position: absolute;
  }
</style>
<div class="penguin">
  <div class="penguin-bottom">
    <div class="right-hand"></div>
    <div class="left-hand"></div>
    <div class="right-feet"></div>
    <div class="left-feet"></div>
  </div>
  <div class="penguin-top">
    <div class="right-cheek"></div>
    <div class="left-cheek"></div>
    <div class="belly"></div>
    <div class="right-eye">
      <div class="sparkle"></div>
    </div>
    <div class="left-eye">
      <div class="sparkle"></div>
    </div>
    <div class="blush-right"></div>
    <div class="blush-left"></div>
    <div class="beak-top"></div>
    <div class="beak-bottom"></div>
  </div>
</div>
```

---

> Basic CSS: Use a custom CSS Variable

- After creating a variable, its value can be assigned to other CSS properties by referencing its name: `background: var(--element-one)`

---

> Basic CSS: Attach a Fallback value to a CSS Variable

- When using variables as a CSS property value, fallback values can be attached that the browser will revert to if the given variables invalid.<break>
  **NOTE**: This browser fallback is not used to increase browser compatibility, and it will not work on IE browser. Rather, it is used so that the browser has a color to display if it cannot find the variable. <break>
  example: `backgroung: var(--element-one, black)`

---

> Basic CSS: Improve Compatibility with Browser Fallbacks

- When working with CSS Browser compatibility issues are likely to be encountered at some point.
- This is why it's important to provide browser fallbacks to avoid potential problems
- When a browser parses the CSS of a webpage, it ignores any properties that it doesn't recognize or support.
- For example Internet Explorer ignores CSS variables, as it does not support CSS variables.
- In case of lack of property support the browser will use whatever value it has for that property. If it can't any other value set for that property, it will revert to the default value, which is not ideal.
- Setting a fallback value is as easy as providing a widely supported value immediately before a declaration.

---

> Baic CSS: Inherit CSS Variables

- Created variables are available inside the selector in which they are created.
- It also is available in any of the selectors descendents.
- This is because CSS variables are inherited, just like ordinary properties.
- To make use of inheritance CSS variables are often defined in the _:root_ element.
- _:root_ is a _pseudo-class_ selector that matches the root element of the document, usually the _html_ element.
- By creating variables in _:root_, they'll be available globally and can be accessed from any other selector in the style sheet.

---

Basic CSS: Change a variable for a specific area

- Variables created in _:root_ set the value of that variables for the whole page.
- To over-write these variables set them againg within a specific element.

---

> Basic CSS: Use a media query to change a variable

- CSS variables can simplify the way media queries are used.
- For instance, when a screen is smaller or larger that the media query break point, the variable value can be changed, and it will apply its style wherever it is used.

---

## Introduction to the Applied Visual Design Challenges

---

> Applied Visual Design: Create Visual Balance Using the text-align Property

- CSS aligns text with the text-align property <break>
  - examples: `text-align: justify;` cause all lines of text except the last line to meet the left and the right edges of the line box. `text-align: center;` centers the text. `text-align: right;` right-aligns the text and `text-align: left;` (the default) left-aligns the text.

---

> Applied Visual Design: Adjust the Width of an Element Using the Width Property

- The _width_ property in CSS specifies an element's width.
- Value can be given in relative length units (such as em), absolute length units (such as px), or as a percentage of its containing parent element. This example changes the width of an image to 220px:

```
img {
  width: 220px;
}
```

---

> Applied Visual Design: Adjust the Height of an Element Using the height Property

- The _height_ property in CSS is similar to the _width_ property and specifies an element's height. This example change the height of an image to 20px:

```
img {
  height: 20px;
}
```

---

> Applied Visual Design: Use the strong Tag to Make Text Bold

- The _strong_ tag is used to make text bold.

---

> Applied Visual Design: Use the u Tag to Underline Text

- The _u_ tag is used to underline text.
- This is often used to signify that a section of text is important, or something to remember.
- With the _u_ tag the browser applies the CSS style of `text-decoration: underline;` to the element.

---

> Applied Visual Design: Use the em Tag to Italicize Text

- The _em_ tag is used to italicize text.
- The browser applies a CSS style `font-style: italic;` to the element.

---

> Applied Visual Design: Use the s Tag to Strikethrough Text

- The _s_ tag is used to strikethrough text with a horizontal line that cuts across characters.
- This tag shows that a section is no longer valid.
- The browser applies the CSS of `text-decoration: line-through;` to the element.

---

> Applied Visual Design: Create a Horizontal Line Using the hr Element

- The _hr_ tag is used to add a horizontal line across the width of its containing element.
- This is useful in indicating a change in topic or to visually separate groups of content.

---

> Applied Visual Design: Adjust the background-color Property of Text

- Instead of adjusting the overall background or the color of the text to make the foreground easily readable, add a _background-color_ to the element holding the text to be emphasized.

```
rgba stands for:
  r = red
  g = green
  b = blue
  a = alpha/level of opacity
```

- The rgb values can range from 0 to 255 and the alpha value can range from 1 which is fully opaque or a solid color, to 0, which is fully transparent or clear.
- _rgba_ is useful for adjusting the opacity of an element.
