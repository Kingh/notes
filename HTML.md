﻿# HTML
-----------------------------
-----------------------------

> Basic HTML and HTML5: Introduction to HTML5 Elements

- HTML5 introduces more descriptive HTML tags:

     - main
     - header
     - footer
     - nav
     - video
     - article
     - section
     - e.t.c
 -----------
 > Basic HTML and HTML5: Adding Images to Website
 
 - the _**img**_ element adds images to a website.
 - to point to an image's url use the _src_ attribute <break>
 _**img**_ element example: `<img src="https://www.image-source.com/image.jpg>`
 - the img element is self enclosing
 - all img elements must have an _alt_ attribute <break>
 **NOTE:** If the image is purely decorative, using an empty _alt_ attribute is a best practice.
 - Ideally the _alt_ attribute should not contain special characters unless needed
 ---------------------------------------------------------------------------------------------------------
 > Basic HTML and HTML5: Link to External Pages with Anchor Elements
 
 - _**a**_(anchor) elements link to contents outside and within a web page
 - _**a**_ elements need a destination web address called an _href_ attribute. They also need anchor text. <break>
 _**a**_ element: `<a href="https://freecodecamp.org">this links to freecodecamp.org</a>`
 - to create an internal link, assign a link's _href_ attribute to a hash symbol plus the value of the _id_ attribute for the element to internally link to, usually further down the page. Then add the same _id_ attribute to the element linked to. An _id_ is an attribute that uniquely describes an element.
 internal _**a**_ element example: 
 
```
 <a href="#contacts-header">Contacts</a>
 . . .
 <h2 id="contacts-header">Contacts</h2>
```
------------------------------------------------------------------------
 > Basic HTML and HTML5: Nest an Anchor Element within other elements
 
 - Links can be nested within other text elements
 
 example:
 
```
 <p>
    Here's a <a target="_blank" href="http://freecodecamp.org"> link to freecodecamp.org</a> for you to follow.
 </p> 
```
 
 --------
> Basic HTML and HTML5: Make Dead Links Using the Hash Symbol

- sometimes _**a**_ elements are added before knowing where they will link, the _#_ symbol serves as a placeholder. <break>

example: `href="#"`

---------------------------
> Basic HTML and HTML5: Turn an Element into a link

- Elements can be turned into  links by nesting them within a _**a**_ element. <break>
example:

```
<a href="#">
   <img src="https://bit.ly/fcc-running-cats" alt="Three kittens running towards the camera.">
</a>
```
----------------------
> Basic HTML and HTML5: Create a Bulleted Unordered List

- HTML has a special element for creating _unordered lists_, or bullet point style lists.
- Unordered lists start with an opening `<ul>` element, followed by any number of `<li>` elements. Finally, unordered lists close with a `</ul>` <break>
example:

```
    <ul>
  	  <li>milk</li>
  	  <li>cheese</li>
    </ul>
```

---------
> Basic HTML and HTML5: Create an ordered List

- HTML has another special element for creating ordered lists, or numbered lists.
- ordered lists start with an opening `<ol>` element, followed by any number of `<li>` elements. Finally, ordered lists are closed with the `</ol>` tag. <break>
example:

```
     <ol>
  	  <li>milk</li>
  	  <li>cheese</li>
    </ol>
```

 ---------
 > Basic HTML and HTML5: Create a Text Field
 
 - _**input**_ elements are a convenient way to get input from a user. <break>
 _**input**_  example:
 `<input type="text">`
 - Placeholder text is what is displayed in your _**input**_ element before a user has inputted anything <break>
 _placeholder_ example:
 `<input type="text" placeholder="this is placeholder text">`
 --------------------------------------------------------------------------------
 > Basic HTML and HTML5: Create a Form Element
 
 - Web forms that submit data to a server can be built using nothing but pure HTML.
 - To do this specify an action in the _**form**_ element. <break>
 example:
 `<form action="/url-where-you-want-to-submit-form-data"></form>`
 --------------------------------------------------------------------------------------------
 > Basic HTML and HTML5: Add a Submit Button to a Form
 
 - Clicking a _submit_ button sends data from a form. <break>
 _submit_ button example:
 `<button type="submit">this button submits the form</button>`
 ---------------------------------------------------------------------------------------
 > Basic HTML and HTML5: Use HTML5 to Require a FieldPassed
 
 - A a form field can be required so that a user will not be able to submit the form until the field has been filled out.
 - to make a form field required, add the attribute _required_ within the input element like this:
 `<input type="text" required>` 
 ------------------------------------------
 > Basic HTML and HTML5: Create a Set of Radio Buttons
 
 - _radio_ buttons can be used for questions where a single answer out of multiple options is required from the user.
 -  Each  _radio_ button can be nested within its own _**label**_ element. By wrapping the  _**input**_ element inside of a _**label**_ element it will automatically associate the _radio_ button input with the label element surrounding it.
 - All related _radio_ buttons should have the same _name_ attribute to create a _radio_ button group.
 - In a _radio_ button group selecting any one of the _radio_ button  will automatically  deselect the other buttons in the same group. <break>
 _radio_ buttons example:
 
```
 <label> 
  <input type="radio" name="indoor-outdoor">Indoor 
</label>
```

- It is considered best practice to set a _for_ attribute on the _**label**_ element, with a value that matches the value of the _id_ attribute of the _**input**_ element. This allows assistive technologies to create a linked relationship between the label and the child _**input**_ element. <break>
example: 

```
<label for="indoor"> 
  <input id="indoor" type="radio" name="indoor-outdoor">Indoor 
</label>
```
------------
> Basic HTML and HTML5: Create a Set of Checkboxes

- Forms commonly use checkboxes for questions that may have more than one answer.
- Checkboxes are a type of _**input**_.
- Each of the checkboxes can be nested within its own _**label**_ element. By wrapping the  _**input**_ element inside of a _**label**_ element it will automatically associate the _checkbox_ input with the label element surrounding it.
- all related checkboxes should have the same _name_ attribute.<break>
- It is considered best practice to set a _for_ attribute on the _**label**_ element, with a value that matches the value of the _id_ attribute of the _**input**_ element. This allows assistive technologies to create a linked relationship between the label and the child _**input**_ element. <break>
example: 

```
<label for="loving"> 
  <input id="loving" type="checkbox" name="personality">Loving 
</label>
```
------------
 > Basic HTML and HTML5: Use the value attribute with Radio Buttons and Checkboxes
 
 - When a from get submitted, the data is sent to the server and includes entries for the options selected. Inputs of type _radio_ and _checkbox_ report their values from the _value_ attribute. <break>
 for example:
 
```
<label for="indoor"> 
  	<input id="indoor" value="indoor" type="radio" name="indoor-outdoor">Indoor 
</label>
<label for="outdoor"> 
  	<input id="outdoor" value="outdoor" type="radio" name="indoor-outdoor">Outdoor 
</label>
```
- the above example is two _radio_ inputs. When the user submits the form with the _indoor_ option
selected, the form data will include the line:_indoor-outdoor=indoor_. This is from the _name_ and _value_ attribute of the "indoor" input.
- if the _value_ attribute is omitted, the submitted form data uses the default value, which is on. In the above scenario, if the user clicked the "indoor" option and submitted the form, the resulting form data would be _indoor-outdoor-on, which is not useful.
------------------------------------------------------------------------
> Basic HTML and HTML5: Check Radio Buttons and Checkboxes by Default

- The _checkbox_ and _radio_ button can be checked by default using the _checked_ attribute.
- To do this, just add the word "checked" to the inside of the input element. <break>
for example:
`<input type="radio" name="test-name" checked>`
---------------------------------------------------------------------
> Basic HTML and HTML5: Nest Many Elements within a Single div Element

- The _**div**_ element, also known as the division element, is a general purpose container for other elements.
_**div**_ example:
`<div></div>`
-------------------
> Basic HTML and HTML5: Declare the Doctype of an HTML Document

- The `<!DOCTYPE ...>` tag specified on the first line of an HTML file tells the browser which version of HTML a page is using. The **...**  part is the version of HTML. for HTML5 `<!DOCTYPE html>` is used.
- the ! and uppercase _DOCTYPE_ is important, especially for older browsers. The _html_ is not case sensitive 
- the rest of the HTML code must be wrapped in _**html**_ tags. The opening `<html>` goes directly below the `<!DOCTYPE html>` line, the closing `</html>` goes at the end of the page.<break>
for example:

```
<!DOCTYPE html>
<html>
  <!-- Your HTML code goes here -->
</html>
```
-----------
> Basic HTML and HTML5: Define the Head and Body of an HTML Document

- The _**head**_ and _**body**_ tag nested within the _**html**_ add another level of organisation in the HTML document
- the _**head**_ tag holds any markup with information about the page, i.e _**link**_, _**meta**_, _**title**_ and _**style**_.
- the _**body**_ tag holds any markup with the content of the page (what is displayed to a user)<break>
example:

```
<!DOCTYPE html>
<html>
  <head>
    <!-- metadata elements -->
  </head>
  <body>
    <!-- page contents -->
  </body>
</html>
```


