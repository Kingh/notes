# Angular

---

> Angular is a JavaScript Framework that allows for the creation of reactive Single-Page-Applications

### Installing Angular

- run `npm install -g @angular/cli` to install the Angular CLI globally on your system.
- to create a project run `ng new my-project-name`
- `ng serve` start the local Angular development server.

---

### First Application

- Angular allows for the mixing of static HTML code and dynamic content
- Directives i.e `[(ngModel)]`, are markers on a DOM element (such as an attribute, element name, comment or CSS class) that tell AngularJS's HTML compiler (\$compile) to attach a specified behavior to that DOM element (e.g. via event listeners), or even to transform the DOM element and its children.
- To add packages to an Angular application, specify the imports in the _app.module.ts_ file.

```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';s
import {FormsModule } from '@angular/forms'; // specifies package for TypeScript

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, // specifies feature for Angular
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

---

### TypeScript

- Is a superset of JavaScript that offers more features than vanilla JS e.g Type, Classes, Interfaces, ...

> Installing bootstrap in Angular

- run `npm install --save bootstrap@version` this installs bootstrap local to the project.
- To be able to use bootstrap add the following line to the angular.json file under _**styles**_ `node_modules/bootstrap/dist/css/bootstrap.min.css`

---

## Angular Basics

- _index.html_ is the single page served by the server.

- Components are loaded into _index.html_ using a selector specified in the _component.ts_ file, the selector is used to create custom HTML tags inside the body tag of _index.html_.

- When the _index.html_ file is served, the Angular CLI inject JavaScript script bundles automatically to enable Angular code.

- The first code to be executed is the code in _main.ts_, which does ENV checks, imports and bootstraps the main/app module.

- In the main/app module the component to be bootstrapped is specified within the _@NgModule_. This allows Angular to load the setup in the main/app component.ts file.

### Modules

- An Angular application has atleast one root module called app module by convention.

- Modules are made up of components and services.

- An Angular application is a collection of individual modules. [![modules]()](https://youtu.be/mDoHtD1hI3I?list=PLC3y8-rFHvwhBRAgFinJR8KHIrCdTkZcZ&t=131)

- An Angular module is lines of code that can be exported or imported.

- Modules interact and ultimately render the view in the browser

### Components

- Components are building blocks of Angular applications.

- A component controls a portion of the view on the browser.

- A component is made up of three parts:

  - Template -> which represents the view (HTML)
  - Class -> code created using TypeScript for controlling the logic of the view.
  - Metadata -> Information needed by Angular to determine if the particular class is an Angular component or just a regular class. The metadata is defined using a decorator, a decorator is a function that provides information about the class it is attached to. For components the decorator is @Component.
  - In the @Component metadata there is a selector, which is used as a custom HTML tag that is used to represent a given component. When specifying a selector in HTML Angular renders the component's template in the place of the selector.
  - A selector can be used as a CSS class as well, begin the selector with a period to use it as a class: `selector: ".app-test",`. The final way to specify a selector is to enclose it within square brackets: `selector: "[app-test]",` this allows for the component to be specified as an attribute in an HTML tag.

- The main/app component is the root component that holds the entire application. This root component will nest the other components created in the application.

### Creating a new component

- Angular uses modules to bundle different pieces of the application into bundles. After creating a new component it must be registered in the @NgModule under the declarations object and import it at the top of the TypeScript file.

- running `ng generate component 'component-name'` create a component with all its parts.

### Services

- Services are classes that contain the applications business logic.

### Interpolation

- Interpolation is the syntax of a property or expression within a pair curly braces:

```
template: `
  <h2>Welcome {{ name }}</h2> // data binding
  <h2>{{ 2 + 2 }}</h2> // Expression evaluation
  <h2>{{ "Welcome " + name}} </h2> // String concatenation
  <h2>{{ name.length }} </h2> // function calls
  <h2>{{ this.greetUser() }} // Internal component method call
  <h2>{{ a = 2 + 2 }}</h2> // Assignments are illegal
  <h2>{{ window.location.href }} // Global JavaScript variables are illegal and unknown to components template
  <h2>{{ siteUrl }}</h2> // legal
  `
...
public name = "name";
public siteUrl = window.location.href;

greetUser() {
  return "Hello " + this.name;
}
```

- Interpolation enables for dynamic data binding from class to template.

### Property Binding

- Attributes and Properties are not the same
- Attributes are defined by HTML
- Properties are defined by the DOM (Document Object Model)
- Attributes initialize DOM properties and then they are done. Attributes values cannot change once initialized.
- Property values can change

- Property binding example:

```
template: `
  <input [id]="myId" type="text" value="test">
  <input [disable]="isDisabled" [id]="myId" type="text" value="test">
  <input bind-disable="isDisabled" [id]="myId" type="text" value="test"> // same as above
`
...
public myId = "testId";
public isDisabled = true;

```

## Property Binding vs Interpolation

- Property binding can be used to replace string interpolation i.e.:

```
template: `
  <p>{{ property }} </p> // String interpolation
  <p [innerText="property"></p> // This is equivalent to the above string interpolation
`
....
public property = "some string";
```

- Property binding is needed because Interpolation can only work with string values.

## Class Binding

- Class binding example:

```
template: `
  <h2 [class]="successClass">CodeSymphony</h2> <!-- class binging-->
  <h2 [class.text-danger]="hasError">CodeSymphony</h2> <!-- conditional class binding-->
`,
style: [`
  .text-success {
    color: green;
  }
  .text-danger {
    color: red;
  }
  .text-special {
    font-style: italic;
  }
`]
...
public successClass = "text-success";
public hasError = true;
```

- A regular class attribute becomes a dummy attribute in the presence of class binding. Use one or the other not both.

- To conditionally apply multiple classes Angular provides the @NgClass directive:

```
template: `
  <h2 [ngClass]="messageClasses">CodeSymphony</h2> <!-- multi iconditional class binding-->
`,
style: [`
  .text-success {
    color: green;
  }
  .text-danger {
    color: red;
  }
  .text-special {
    font-style: italic;
  }
`]
...
public hasError = false;
public isSpecial = true;
public messageClasses = {
  text-success: !this.hasError;
  text-danger: !this.hasError;
  text-special: this.isSpecial;
}
```

---

## Event Binding

- For event binding use parenthesis and put the event name between the parenthesis:

```
<button (event_name)="eventHandler">Button Text</button>
```

- To get the data of an event pass the `$event` object to the _eventHandler_
- Passing data from view to component using the `$event` object:

```
// component.html
<input
  type="text"
  (input)="onInputEvent($event)" // pass event object to component method
  >
  ....
//component.ts
....
onInputEvent(event) {
  this.var = (<HTMLInputElement>event.target).value; //type cast the event parameter
}
....
```

---

## Directives

- Directives are instructions in the DOM!
- An asterisk at the beginning of a directive indicates that a directive is a structural directive, meaning it add or removes elements from the DOM: `<p *ngIf="true/false condition></p>`
- Another type of directive is the attribute directive, attribute directives change the element they are placed on.
